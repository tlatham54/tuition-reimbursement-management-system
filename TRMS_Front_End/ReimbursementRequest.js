//playground



//INITIALIZATION

let userID = getUserID();
let requestID = getRequestID();
//this is passed in from browser

let user;
let request;
let userRole; 
let informationRequests;

populateForm();



//INIT FUNCTIONS

function getUserID() {
    if (!supportsLocalStorage()) {return false;}
    return parseInt(localStorage["userID"]);
}

function getRequestID() {
    if (!supportsLocalStorage()) {return false;}
    return parseInt(localStorage["requestID"]);
}

async function getPertinentInformation() {

    user = await getUser();
    request = await getRequest();
    informationRequests = await getInformationRequests();

    {//TEST DATA
    

    // fullRequest = {
    //     requestID: 1,
    //     trackingNumber: 6039,   //not in DB
    //     creator: {
    //         userID: 0,
    //         username: undefined,
    //         password: undefined,
    //         firstName: undefined,
    //         lastName: undefined,
    //         department: {
    //             departmentID: undefined,
    //             departmentName: undefined
    //         },
    //         supervisor: {
    //             userID: 1,
    //             username: undefined,
    //             password: undefined,
    //             firstName: undefined,
    //             lastName: undefined,
    //             department: undefined,
    //             supervisor: 1,
    //             departmentHead: 2 //not in DB
    //         },
    //         departmentHead: { //not in DB
    //             userID: 2,
    //             username: undefined,
    //             password: undefined,
    //             firstName: undefined,
    //             lastName: undefined,
    //             department: undefined,
    //             supervisor: undefined,
    //             departmentHead: undefined
    //         } 
    //     },
    //     event: {
    //         eventID: 1,
    //         location: "Salzberg",
    //         date: "2021-10-18",
    //         time: "16:14",
    //         eventType: {
    //             eventTypeID: undefined,
    //             typeName: "Seminar",
    //             reimbursementCoverage: undefined
    //         },
    //         gradingFormat: {
    //             formatID: undefined,
    //             type: "GPA"
    //         },
    //         passingGrade: "2.0",
    //         description: "Just a course about JavaScript",
    //         cost: 250.05,
    //         missedWork: "Tuesdays for a month"
    //     },
    //     finalGradeOrPresentation: "someLink", //"finalGradeOrPresentationLink.link",
    //     assignedBC: {
    //         userID: 3,
    //         username: undefined,
    //         password: undefined,
    //         firstName: undefined,
    //         lastName: undefined,
    //         department: undefined,
    //         supervisor: undefined,
    //         departmentHead: undefined
    //     },
    //     approvedAmount: undefined,

    //     status: {
    //         statusID: 0,
    //         statusName: undefined
    //     },

    //     dateCreated: "2021-10-16",
    //     dateLastApproved: undefined,

    //     firstName: "Sven",
    //     lastName: "Dorgun",
    //     department: "Accounting",
    //     justification: "You know accountants really need JavaScript",
    //     urgent: undefined,
    //     projectedReimbursement: 250, //not in DB
    //     amountMoreThanAvailable: undefined
    // };

    // request = fullRequest;

    // informationRequests = [
    //     {
    //         requestID: 1,
    //         informationRequestID: 1,
    //         requestDate: "2021-01-23",
    //         requestingParty: {
    //             userID: 3,
    //             username: undefined,
    //             password: undefined,
    //             firstName: undefined,
    //             lastName: undefined,
    //             department: undefined,
    //             supervisor: 1,
    //             departmentHead: 2 //not in DB
    //         },
    //         requestBody: "Request BenCo -> Creator",
    //         respondingParty: {
    //             userID: 0,
    //             username: undefined,
    //             password: undefined,
    //             firstName: undefined,
    //             lastName: undefined,
    //             department: undefined,
    //             supervisor: 1,
    //             departmentHead: 2 //not in DB
    //         },
    //         responseBody: "Response Creator -> Benco"
    //     },

    //     {
    //         requestID: 1,
    //         informationRequestID: 2,
    //         requestDate: "2021-02-23",
    //         requestingParty: {
    //             userID: 3,
    //             username: undefined,
    //             password: undefined,
    //             firstName: undefined,
    //             lastName: undefined,
    //             department: undefined,
    //             supervisor: 1,
    //             departmentHead: 2 //not in DB
    //         },
    //         requestBody: "Request BenCo -> department head",
    //         respondingParty: {
    //             userID: 2,
    //             username: undefined,
    //             password: undefined,
    //             firstName: undefined,
    //             lastName: undefined,
    //             department: undefined,
    //             supervisor: 1,
    //             departmentHead: 2 //not in DB
    //         },
    //         responseBody: "Response DH -> Benco"
    //     },

    //     {
    //         requestID: 1,
    //         informationRequestID: 3,
    //         requestDate: "2021-03-23",
    //         requestingParty: {
    //             userID: 2,
    //             username: undefined,
    //             password: undefined,
    //             firstName: undefined,
    //             lastName: undefined,
    //             department: undefined,
    //             supervisor: 1,
    //             departmentHead: 2 //not in DB
    //         },
    //         requestBody: "Request department head -> direct supervisor",
    //         respondingParty: {
    //             userID: 1,
    //             username: undefined,
    //             password: undefined,
    //             firstName: undefined,
    //             lastName: undefined,
    //             department: undefined,
    //             supervisor: 1,
    //             departmentHead: 2 //not in DB
    //         },
    //         responseBody: "Response DS -> Benco"
    //     },

    //     {
    //         requestID: 1,
    //         informationRequestID: 4,
    //         requestDate: "2021-01-23",
    //         requestingParty: {
    //             userID: 3,
    //             username: undefined,
    //             password: undefined,
    //             firstName: undefined,
    //             lastName: undefined,
    //             department: undefined,
    //             supervisor: 1,
    //             departmentHead: 2 //not in DB
    //         },
    //         requestBody: "Request BenCo -> Creator",
    //         respondingParty: {
    //             userID: 0,
    //             username: undefined,
    //             password: undefined,
    //             firstName: undefined,
    //             lastName: undefined,
    //             department: undefined,
    //             supervisor: 1,
    //             departmentHead: 2 //not in DB
    //         },
    //         responseBody: ""
    //     },

    //     {
    //         requestID: 1,
    //         informationRequestID: 5,
    //         requestDate: "2021-01-23",
    //         requestingParty: {
    //             userID: 2,
    //             username: undefined,
    //             password: undefined,
    //             firstName: undefined,
    //             lastName: undefined,
    //             department: undefined,
    //             supervisor: 1,
    //             departmentHead: 2 //not in DB
    //         },
    //         requestBody: "Request DH -> Creator",
    //         respondingParty: {
    //             userID: 0,
    //             username: undefined,
    //             password: undefined,
    //             firstName: undefined,
    //             lastName: undefined,
    //             department: undefined,
    //             supervisor: 1,
    //             departmentHead: 2 //not in DB
    //         },
    //         responseBody: ""
    //     }
    // ];
    }

    if (request.requestID) {

        this.userRole = getUserRole(userID);
    }
}

async function getUser() {

    const URL = "http://localhost:7000/users/" + userID;

    const httpResponse = await fetch(URL);

    return await httpResponse.json();
    
}

async function getRequest() {

    if(requestID) {

        const URL = "http://localhost:7000/requests/" + requestID;

        const httpResponse = await fetch(URL);

        return await httpResponse.json();

    } else {

        return {
            creatorFE: {
                userID: undefined,
                username: undefined,
                password: undefined,
                firstName: undefined,
                lastName: undefined,
                department: {
                    departmentID: undefined,
                    departmentName: undefined
                },
                supervisor:{
                    userID: undefined,
                    username: undefined,
                    password: undefined,
                    firstName: undefined,
                    lastName: undefined,
                    department: undefined,
                    supervisor: undefined,
                },
                departmentHead: {
                    userID: undefined,
                    username: undefined,
                    password: undefined,
                    firstName: undefined,
                    lastName: undefined,
                    department: undefined,
                    supervisor: undefined,
                }
            },
            eventFE: {
                eventID: undefined,
                location: undefined,
                dateString: undefined,
                timeString: undefined,
                eventType: {
                    eventTypeID: undefined,
                    typeName: undefined,
                    reimbursementCoverage: undefined
                },
                gradingFormat: {
                    formatID: undefined,
                    type: undefined
                },
                passingGrade: undefined,
                description: undefined,
                cost: undefined,
                missedWork: undefined,
                dateTime: undefined
            },
            fgopString: undefined,
            requestID: undefined,
            trackingNumber: undefined,
            creator: {
                userID: undefined,
                username: undefined,
                password: undefined,
                firstName: undefined,
                lastName: undefined,
                department: undefined,
                supervisor: undefined,
            },
            event: {
                eventID: undefined,
                location: undefined,
                eventType: {
                    eventTypeID: undefined,
                    typeName: undefined,
                    reimbursementCoverage: undefined
                },
                gradingFormat: {
                    formatID: undefined,
                    type: undefined
                },
                passingGrade: undefined,
                description: undefined,
                cost: undefined,
                missedWork: undefined,
                dateTime: undefined
            },
            finalGradeOrPresentation: undefined,
            fgopFileName: undefined,
            assignedBC: {
                userID: undefined,
                username: undefined,
                password: undefined,
                firstName: undefined,
                lastName: undefined,
                department: undefined,
                supervisor: undefined,
            },
            approvedAmount: undefined,

            status: {
                statusID: undefined,
                statusName: undefined
            },

            dateCreated: undefined,
            denialReason: undefined,

            firstName: undefined,
            lastName: undefined,
            department: undefined,
            justification:undefined,
            isUrgent: undefined,
            projectedReimbursement: undefined,
            amountMoreThanAvailable: undefined
        }
    }

    

}

async function getInformationRequests() {

    const URL = "http://localhost:7000/informationRequests/";

    const httpResponse = await fetch(URL);

    let allInformationRequests = await httpResponse.json();

    let pertinentInformationRequests = [];

    for (ir of allInformationRequests) {

        console.log(ir.request.requestID);
        console.log(request);


        if (ir.request.requestID == requestID) {

            pertinentInformationRequests.push(ir);
        }
    }

    return pertinentInformationRequests;

}

function getUserRole(userID) {
    if(userID == request.creatorFE.userID) {
        return 0; //creator
    } else if(userID == request.creatorFE.supervisor.userID) {
        return 1; //direct supervisor
    } else if (userID == request.creatorFE.departmentHead.userID) {
        return 2;   //department head
    } else if (userID == request.assignedBC.userID) {
        return 3;   //benco
    } else {
        return "unauthorized";
    }
}

async function populateForm() {

    await getPertinentInformation();

    console.log(request);
    userRole = getUserRole(userID);

    if (!(request.requestID)) {

        //then we assume this is a brand new request

        document.getElementBy

        document.getElementById("creatorActions").classList.remove("invisible");
        document.getElementById("finalGradeCol").classList.add("invisible");


        populatePassingGrade();
        useDefaultPassingGrade();

    
    } else if (request.status.statusID == 1) {

        //populate form with read-only information in the General Information section
        populateGeneral();
        populateInformation();

        if (userRole == 0) {

        } else if (userRole == 1) {

            document.getElementById("approverActions").classList.remove("invisible");
            document.getElementById("inputApprovedAmount").classList.add("invisible");
            document.getElementById("inputApprovedAmountLabel").classList.add("invisible");

        }

    } else if (request.status.statusID == 2) {

        populateGeneral();
        populateInformation();


        if (userRole == 0) {

        } else if (userRole == 1) {
            
        } else if (userRole == 2) {

            document.getElementById("approverActions").classList.remove("invisible");
            document.getElementById("inputApprovedAmount").classList.add("invisible");
            document.getElementById("inputApprovedAmountLabel").classList.add("invisible");

        }
        
    } else if (request.status.statusID == 3) {
            
        populateGeneral();
        populateInformation();

        if (userRole == 0) {

        } else if (userRole == 1) {
            
        } else if (userRole == 2) {
            
        } else if (userRole == 3) {

            document.getElementById("approverActions").classList.remove("invisible");
            const approvedAmountInput = document.getElementById("inputApprovedAmount");
            approvedAmountInput.value = document.getElementById("inputProjectedReimbursement").value;
        }
        
    } else if (request.status.statusID == 4 && !request.fgopFileName) {

        populateGeneral();
        populateInformation();


        if (userRole == 0) {

            populateFinalGrade();
            document.getElementById("creatorActions").classList.remove("invisible");
            document.getElementById("submitCol").classList.add("invisible");
        }
        
    } else if (request.status.statusID == 4 && request.fgopFileName) {

        populateGeneral();
        populateInformation();


        if (userRole == 0) {


        } else if (userRole == 1) {

            document.getElementById("finalActions").classList.remove("invisible");
            document.getElementById("passingGradeCol").classList.add("invisible");
            finalGradeInput = document.getElementById("inputGradeOrPresentation");
            finalGradeInput.value = request.fgopFileName;
            
        } else if (userRole == 3) {

            document.getElementById("finalActions").classList.remove("invisible");
            document.getElementById("successfulPresentationCol").classList.add("invisible");
            finalGradeInput = document.getElementById("inputGradeOrPresentation");
            finalGradeInput.value = request.fgopFileName;
        }
        
    } else if (request.status.statusID == 5 || request.status.statusID == 6) {

        populateGeneral();
        populateInformation();
        
    }
}




//BUTTON FUNCTIONS

async function createNewRequest() {

    let container = document.querySelector("#generalInformationSection");

    if(validateFields(container)) {
        errorMessage("");
        buildEvent();
        buildRequest();

        if (user.supervisor.userID != userID) {
            request.status.statusID = 1;
        } else {
            request.status.statusID = 3;
        }

        const url = "http://localhost:7000/requests/";

        const httpResponse = await fetch(url, 
        {
            method: "POST",
            headers: {
                "Content-Type": "application/json"
              },
            body: JSON.stringify(request)
        });
        
        let statusCode = await httpResponse.status;

        if (statusCode == 201) {

            localStorage["requestID"] = undefined;
            window.location.href = "file:///C:/Users/tlath/OneDrive/Documents/Revature/VisualStudio/TRMS/LandingPage.html";

        } else if (statusCode == 400) {

            errorMessage("Event must not have already started.")
            
        } else {

            errorMessage("Something went wrong.");
        }

    } else {
        errorMessage("Highlighted fields must not be empty.");
    }
}

async function enterFinalGrade() {

    const finalGradeCol = document.getElementById("finalGradeCol");

    if (validateFields(finalGradeCol)) {

        let fgopFileName = document.getElementById("inputFinalGradeOrPresentation").value;

        request.fgopFileName = fgopFileName;

        const url = "http://localhost:7000/requests/";

        const httpResponse = await fetch(url, 
        {
            method: "PUT",
            headers: {
                "Content-Type": "application/json"
                },
            body: JSON.stringify(request)
        });
        
        let statusCode = await httpResponse.status;

        if (statusCode == 200) {

            localStorage["requestID"] = undefined;
            window.location.href = "file:///C:/Users/tlath/OneDrive/Documents/Revature/VisualStudio/TRMS/LandingPage.html";

        } else {

            errorMessage("Something went wrong.");
        }  

    } else {

        errorMessage("Must select a file to upload.")
    }

    
}

async function enterAdditionalInformation() {

    const resCol = document.getElementById("submitInformationCol");

    if(validateFields(resCol)) {

        let resBody = document.getElementById("inputSubmitInformation").value;


        let infoRequest;

        for (ir of informationRequests) {

            const requesterRole = getUserRole(ir.requestingParty.userID);

            if (ir.request.requestID == requestID && !ir.responseBody && request.status.statusID <= requesterRole) {

                ir["responseBody"] = resBody;

                infoRequest = ir;
            }
        }

        const url = "http://localhost:7000/informationRequests/" + infoRequest.informationRequestID;

        const httpResponse = await fetch(url, 
        {
            method: "PUT",
            headers: {
                "Content-Type": "application/json"
            },
            body: JSON.stringify(infoRequest)
        });
        
        let statusCode = httpResponse.status;

        if (statusCode == 200) {

            document.getElementById("submitInformationCol").classList.add("invisible");

        } else {

            errorMessage("Something went wrong.");
        }
    } else {
        
        errorMessage("Must enter a response.")
    }

    
    

    
}

async function requestAdditionalInformation() {
    
    let rc = request.creator;
    let ds = request.creator.supervisor;
    let dh = request.creatorFE.departmentHead;

    let informationRequest = {
        informationRequestID: undefined,
        request: request,
        requestingParty: user,
        respondingParty: undefined,
        requestBody: undefined,
        responseBody: undefined,
        requestDate: Date.now()
    }

    let reqBod = document.getElementById("inputRequestInformation").value;
    informationRequest.requestBody = reqBod;

    let rp = document.getElementById("inputRespondingParty").value;
    
    if (rp == "Direct Supervisor") {

        informationRequest.respondingParty = ds;

    } else if (rp == "Department Head") {

        informationRequest.respondingParty = dh;

    } else if (rp == "Request Creator") {

        informationRequest.respondingParty = rc;
    }


    let infoCol = document.getElementById("requestInformationCol");

    if (rp == "Please select...") {

        errorMessage("Must select a valid responding party.")

    } else if (!validateFields(infoCol)) {

        errorMessage("Must provide information request details.")

    } else {
        
        const url = "http://localhost:7000/informationRequests/";

        const httpResponse = await fetch(url, 
        {
            method: "POST",
            headers: {
                "Content-Type": "application/json"
              },
            body: JSON.stringify(informationRequest)
        });
        
        let statusCode = httpResponse.status;

        if (statusCode == 201) {

            if (rp == "Direct Supervisor") {

                document.getElementById("requestInfoFrom1").classList.add("invisible");
                document.getElementById("inputRespondingParty").value = "Please select...";
        
            } else if (rp == "Department Head") {
        
                document.getElementById("requestInfoFrom2").classList.add("invisible");
                document.getElementById("inputRespondingParty").value = "Please select...";
        
            } else if (rp == "Request Creator") {

                document.getElementById("requestInfoFrom0").classList.add("invisible");
                document.getElementById("inputRespondingParty").value = "Please select...";
            }
        } else {

            errorMessage("Something went wrong.");
        }
    }

}

async function denyRequest() {
    
    if (userRole == 1 || userRole == 2) {

        const denialInput = document.getElementById("denyCol1");

        if (!validateFields(denialInput)) {

            errorMessage("Must give a reason for denial.");

            return false;

        } else {

            request.denialReason = document.getElementById("inputDenyReason").innerHTML;
        }
    }

    request.status.statusID = 6;

    const url = "http://localhost:7000/requests/";

        const httpResponse = await fetch(url, 
        {
            method: "PUT",
            headers: {
                "Content-Type": "application/json"
                },
            body: JSON.stringify(request)
        });
        
        let statusCode = await httpResponse.status;

        if (statusCode == 200) {

            localStorage["requestID"] = undefined;
            window.location.href = "file:///C:/Users/tlath/OneDrive/Documents/Revature/VisualStudio/TRMS/LandingPage.html";

        } else {

            errorMessage("Something went wrong.");
        }

}

async function approveRequest() {

    let reqStatus = request.status.statusID;
    let creatorIsCEO = (request.creatorFE.userID == request.creatorFE.supervisor.supervisorID);
    let isPresentation = (request.eventFE.eventType == "Presentation");
    let dsIsDh = (request.creatorFE.supervisor.userID == request.creatorFE.departmentHead.userID);
    let creatorIsDh = (request.creatorFE.userID == request.creatorFE.departmentHead.userID);

    if (userRole == 1 && reqStatus == 1 && !dsIsDh && !creatorIsDh) {

        request.status.statusID = 2;

    } else if ( 
           (userRole == 1 && reqStatus == 1 && dsIsDh) 
        || (userRole == 1 && reqStatus == 1 && creatorIsDh) 
        || (userRole == 2 && reqStatus == 2)
        ) {
        
        request.status.statusID = 3;

    } else if (userRole == 3 && reqStatus == 3) {

        const approveCol = document.getElementById("approveCol");

        if (validateFields(approveCol)) {

            const approvedAmount = document.getElementById("inputApprovedAmount").value;

            request.approvedAmount = approvedAmount;

            if (creatorIsCEO && isPresentation) {

                request.status.statusID = 5;

            } else {
                request.status.statusID = 4;
            }

            

        } else {

            errorMessage("Must enter an approved amount.")
        } 
    }

    const url = "http://localhost:7000/requests/";

        const httpResponse = await fetch(url, 
        {
            method: "PUT",
            headers: {
                "Content-Type": "application/json"
                },
            body: JSON.stringify(request)
        });
        
        let statusCode = await httpResponse.status;

        if (statusCode == 200) {

            localStorage["requestID"] = undefined;
            window.location.href = "file:///C:/Users/tlath/OneDrive/Documents/Revature/VisualStudio/TRMS/LandingPage.html";

            console.log(request);
        } else {

            errorMessage("Something went wrong.");
        }
}

async function reimburse() {

    request.status.statusID = 5;
    
    const url = "http://localhost:7000/requests/";

        const httpResponse = await fetch(url, 
        {
            method: "PUT",
            headers: {
                "Content-Type": "application/json"
                },
            body: JSON.stringify(request)
        });
        
        let statusCode = await httpResponse.status;

        if (statusCode == 200) {

            localStorage["requestID"] = undefined;
            window.location.href = "file:///C:/Users/tlath/OneDrive/Documents/Revature/VisualStudio/TRMS/LandingPage.html";

            console.log(request);
        } else {

            errorMessage("Something went wrong.");
        }
}



//BUILDER FUNCTIONS

function buildEvent() {

    request.eventFE.location = document.getElementById("inputLocation").value;
    request.eventFE.dateString = document.getElementById("inputDate").value;
    request.eventFE.timeString = document.getElementById("inputTime").value;

    request.eventFE.eventType.typeName = document.getElementById("inputEventType").value;
    if (request.eventFE.eventType.typeName == "University Course") {
        request.eventFE.eventType.eventTypeID = 1;
    } else if (request.eventFE.eventType.typeName == "Seminar") {
        request.eventFE.eventType.eventTypeID = 2;
    } else if (request.eventFE.eventType.typeName == "Certification Preparation Course") {
        request.eventFE.eventType.eventTypeID = 3;
    } else if (request.eventFE.eventType.typeName == "Certification") {
        request.eventFE.eventType.eventTypeID = 4;
    } else if (request.eventFE.eventType.typeName == "Technical Training") {
        request.eventFE.eventType.eventTypeID = 5;
    } else if (request.eventFE.eventType.typeName == "Other") {
        request.eventFE.eventType.eventTypeID = 6;
    }

    request.eventFE.gradingFormat.type = document.getElementById("inputGradingFormat").value;
    if (request.eventFE.gradingFormat.type == "GPA") {
        request.eventFE.gradingFormat.formatID = 1;
    } else if (request.eventFE.gradingFormat.type == "Letter Grade") {
        request.eventFE.gradingFormat.formatID = 2;
    } else if (request.eventFE.gradingFormat.type == "Percentage") {
        request.eventFE.gradingFormat.formatID = 3;
    } else if (request.eventFE.gradingFormat.type == "Pass/Fail") {
        request.eventFE.gradingFormat.formatID = 4;
    } else if (request.eventFE.gradingFormat.type == "Presentation") {
        request.eventFE.gradingFormat.formatID = 5;
    }
    
    request.eventFE.passingGrade = document.getElementById("inputPassingGrade").value;
    request.eventFE.description = document.getElementById("inputDescription").value;
    request.eventFE.cost = document.getElementById("inputCost").value;
    request.eventFE.missedWork = document.getElementById("inputMissedWork").value;
}       

function buildRequest() {
    request.firstName = document.getElementById("inputFirstName").value;
    request.lastName = document.getElementById("inputLastName").value;
    request.department = document.getElementById("inputDepartment").value;
    request.justification = document.getElementById("inputJustification").value;

    request.creatorFE = user;
}



//OTHER HELPER FUNCTIONS

function populateFinalGrade() {

    let formatInput = document.getElementById("inputGradingFormat");

    if (formatInput.selectedIndex == 0) {

        document.getElementById("finalGrade0").innerHTML = "4.0";
        document.getElementById("finalGrade1").innerHTML = "≥3.0";
        document.getElementById("finalGrade2").innerHTML = "≥2.0";
        document.getElementById("finalGrade3").innerHTML = "≥1.0";

    } else if (formatInput.selectedIndex == 1) {

        document.getElementById("finalGrade0").innerHTML = "A";
        document.getElementById("finalGrade1").innerHTML = "≥B";
        document.getElementById("finalGrade2").innerHTML = "≥C";
        document.getElementById("finalGrade3").innerHTML = "≥D";

    } else if (formatInput.selectedIndex == 2) {

        document.getElementById("finalGrade0").innerHTML = "100%";
        document.getElementById("finalGrade1").innerHTML = "≥80%";
        document.getElementById("finalGrade2").innerHTML = "≥70%";
        document.getElementById("finalGrade3").innerHTML = "≥60%";
        
    } else if (formatInput.selectedIndex == 3) {

        document.getElementById("finalGrade0").classList.add("invisible");
        document.getElementById("finalGrade1").classList.add("invisible");
        document.getElementById("finalGrade2").innerHTML = "Pass";
        document.getElementById("finalGrade3").innerHTML = "Fail";
        
    } else if (formatInput.selectedIndex == 4) {

        document.getElementById("finalGrade0").classList.add("invisible");
        document.getElementById("finalGrade1").classList.add("invisible");
        document.getElementById("finalGrade2").innerHTML = "Presentation delivered";
        document.getElementById("finalGrade3").classList.add("invisible");
        
    }

}

function populatePassingGrade() {

    let formatInput = document.getElementById("inputGradingFormat");

    formatInput.addEventListener("change", () => {

        if (formatInput.selectedIndex == 0) {

            document.getElementById("passingGrade0").innerHTML = "4.0";
            document.getElementById("passingGrade1").innerHTML = "≥3.0";
            document.getElementById("passingGrade2").innerHTML = "≥2.0";
            document.getElementById("passingGrade3").innerHTML = "≥1.0";

        } else if (formatInput.selectedIndex == 1) {

            document.getElementById("passingGrade0").innerHTML = "A";
            document.getElementById("passingGrade1").innerHTML = "≥B";
            document.getElementById("passingGrade2").innerHTML = "≥C";
            document.getElementById("passingGrade3").innerHTML = "≥D";

        } else if (formatInput.selectedIndex == 2) {

            document.getElementById("passingGrade0").innerHTML = "100%";
            document.getElementById("passingGrade1").innerHTML = "≥80%";
            document.getElementById("passingGrade2").innerHTML = "≥70%";
            document.getElementById("passingGrade3").innerHTML = "≥60%";
            
        } else if (formatInput.selectedIndex == 3) {

            document.getElementById("passingGrade0").classList.add("invisible");
            document.getElementById("passingGrade1").classList.add("invisible");
            document.getElementById("passingGrade2").innerHTML = "Pass";
            document.getElementById("passingGrade3").innerHTML = "Fail";
            
        } else if (formatInput.selectedIndex == 4) {

            document.getElementById("passingGrade0").classList.add("invisible");
            document.getElementById("passingGrade1").classList.add("invisible");
            document.getElementById("passingGrade2").innerHTML = "N/A";
            document.getElementById("passingGrade3").classList.add("invisible");
            
        }   

    })

    
}

function useDefaultPassingGrade() {
    
    let checkBox = document.getElementById("defaultPassingGrade");

    let formatInput = document.getElementById("inputGradingFormat");

    let passingGradeInput = document.getElementById("inputPassingGrade");

    checkBox.addEventListener("change", () => {
        if (checkBox.checked) {

            for (let i = 0; i < 4; i ++) {
                if (i == 2) {
                    passingGradeInput.selectedIndex = i;
                } else {
                    document.getElementById("passingGrade" + i).classList.add("invisible");
                }
            }
            passingGradeInput.readOnly = true;
        } else {

            passingGradeInput.readOnly = false;

            for (let i = 0; i < 4; i ++) {
                document.getElementById("passingGrade" + i).classList.remove("invisible");
            }

            let formatInput = document.getElementById("inputGradingFormat");

            if (formatInput.selectedIndex == 0) {

                document.getElementById("passingGrade0").innerHTML = "4.0";
                document.getElementById("passingGrade1").innerHTML = "≥3.0";
                document.getElementById("passingGrade2").innerHTML = "≥2.0";
                document.getElementById("passingGrade3").innerHTML = "≥1.0";
    
            } else if (formatInput.selectedIndex == 1) {
    
                document.getElementById("passingGrade0").innerHTML = "A";
                document.getElementById("passingGrade1").innerHTML = "≥B";
                document.getElementById("passingGrade2").innerHTML = "≥C";
                document.getElementById("passingGrade3").innerHTML = "≥D";
    
            } else if (formatInput.selectedIndex == 2) {
    
                document.getElementById("passingGrade0").innerHTML = "100%";
                document.getElementById("passingGrade1").innerHTML = "≥80%";
                document.getElementById("passingGrade2").innerHTML = "≥70";
                document.getElementById("passingGrade3").innerHTML = "≥60";
                
            } else if (formatInput.selectedIndex == 3) {
    
                document.getElementById("passingGrade0").classList.add("invisible");
                document.getElementById("passingGrade1").classList.add("invisible");
                document.getElementById("passingGrade2").innerHTML = "Pass";
                document.getElementById("passingGrade3").innerHTML = "Fail";
                
            } else if (formatInput.selectedIndex == 4) {
    
                document.getElementById("passingGrade0").classList.add("invisible");
                document.getElementById("passingGrade1").classList.add("invisible");
                document.getElementById("passingGrade2").innerHTML = "N/A";
                document.getElementById("passingGrade3").classList.add("invisible");
                
            }   
        }
    });

    formatInput.addEventListener("change", () => {
        if (checkBox.checked) {

            for (let i = 0; i < 4; i ++) {
                if (i == 2) {
                    passingGradeInput.selectedIndex = i;
                } else {
                    document.getElementById("passingGrade" + i).classList.add("invisible");
                }
            }

            passingGradeInput.readOnly = true;
        } else {

            for (let i = 0; i < 4; i ++) {
                document.getElementById("passingGrade" + i).classList.remove("invisible");
            }

            let formatInput = document.getElementById("inputGradingFormat");

            if (formatInput.selectedIndex == 0) {

                document.getElementById("passingGrade0").innerHTML = "4.0";
                document.getElementById("passingGrade1").innerHTML = "≥3.0";
                document.getElementById("passingGrade2").innerHTML = "≥2.0";
                document.getElementById("passingGrade3").innerHTML = "≥1.0";
    
            } else if (formatInput.selectedIndex == 1) {
    
                document.getElementById("passingGrade0").innerHTML = "A";
                document.getElementById("passingGrade1").innerHTML = "≥B";
                document.getElementById("passingGrade2").innerHTML = "≥C";
                document.getElementById("passingGrade3").innerHTML = "≥D";
    
            } else if (formatInput.selectedIndex == 2) {
    
                document.getElementById("passingGrade0").innerHTML = "100%";
                document.getElementById("passingGrade1").innerHTML = "≥80%";
                document.getElementById("passingGrade2").innerHTML = "≥70%";
                document.getElementById("passingGrade3").innerHTML = "≥60%";
                
            } else if (formatInput.selectedIndex == 3) {
    
                document.getElementById("passingGrade0").classList.add("invisible");
                document.getElementById("passingGrade1").classList.add("invisible");
                document.getElementById("passingGrade2").innerHTML = "Pass";
                document.getElementById("passingGrade3").innerHTML = "Fail";
                
            } else if (formatInput.selectedIndex == 4) {
    
                document.getElementById("passingGrade0").classList.add("invisible");
                document.getElementById("passingGrade1").classList.add("invisible");
                document.getElementById("passingGrade2").innerHTML = "N/A";
                document.getElementById("passingGrade3").classList.add("invisible");
                
            }   

        }
    });


}

function validateFields(container) {
    //get all input fields
    fields = container.querySelectorAll("input, select, textarea");

    let allValid = true;

    for (element of fields) {
        if (!element.checkValidity()) {
            allValid = false;
            element.style = "border-color: red";
        } else {
            element.style = "border-color: default";
        }
    }

    return allValid;
}

function errorMessage(message) {
    document.getElementById("errorMessage").innerHTML = message;
}

function populateGeneral() {
    let container = document.querySelector("#generalInformationSection");
    fields = container.querySelectorAll("input, select, textarea");
    document.getElementById("defaultPassingGrade").classList.add("invisible");
    document.getElementById("defaultPassingGradeLabel").classList.add("invisible");

    const eventSection = document.getElementById("eventInformationSection");

    for (element of fields) {

        element.readOnly = true;
        element.style = "background-color: rgb(190, 190, 190)";
        

        propertyString = lowerCaseFirstLetter(element.id.slice(5));

        if (eventSection.contains(element) && element.tageName == "TEXTAREA") {
            element.innerHTML = request.eventFE[propertyString];
        } else if (eventSection.contains(element)) {
            element.value = request.eventFE[propertyString];
        } else if (element.tageName == "TEXTAREA") {
            element.innerHTML = request[propertyString];
        } else {
            element.value = request[propertyString];
        }

        if (element.id == "inputDate") {
            element.value = request.eventFE.dateString;
        } else if (element.id == "inputTime") {
            element.value = request.eventFE.timeString;
        } else if (element.id == "inputEventType") {
            element.value = request.eventFE.eventType.typeName;
        } else if (element.id == "inputGradingFormat") {
            element.value = request.eventFE.gradingFormat.type;
        } else if (element.id == "inputDateCreated") {
            element.value = getDateString(request.dateCreated);
        } else if (element.id == "inputStatus") {
            element.value = request.status.statusName;
        } else if (element.id == "inputPassingGrade") {
            console.log(request.eventFE.passingGrade);
            element.value = request.eventFE.passingGrade;
        }

        let formatInput = document.getElementById("inputGradingFormat");

        if (formatInput.selectedIndex == 0) {

            document.getElementById("passingGrade0").innerHTML = "4.0";
            document.getElementById("passingGrade1").innerHTML = "≥3.0";
            document.getElementById("passingGrade2").innerHTML = "≥2.0";
            document.getElementById("passingGrade3").innerHTML = "≥1.0";

        } else if (formatInput.selectedIndex == 1) {

            document.getElementById("passingGrade0").innerHTML = "A";
            document.getElementById("passingGrade1").innerHTML = "≥B";
            document.getElementById("passingGrade2").innerHTML = "≥C";
            document.getElementById("passingGrade3").innerHTML = "≥D";

        } else if (formatInput.selectedIndex == 2) {

            document.getElementById("passingGrade0").innerHTML = "100%";
            document.getElementById("passingGrade1").innerHTML = "≥80%";
            document.getElementById("passingGrade2").innerHTML = "≥70%";
            document.getElementById("passingGrade3").innerHTML = "≥60%";
            
        } else if (formatInput.selectedIndex == 3) {

            document.getElementById("passingGrade0").classList.add("invisible");
            document.getElementById("passingGrade1").classList.add("invisible");
            document.getElementById("passingGrade2").innerHTML = "Pass";
            document.getElementById("passingGrade3").innerHTML = "Fail";
            
        } else if (formatInput.selectedIndex == 4) {

            document.getElementById("passingGrade0").classList.add("invisible");
            document.getElementById("passingGrade1").classList.add("invisible");
            document.getElementById("passingGrade2").innerHTML = "N/A";
            document.getElementById("passingGrade3").classList.add("invisible");
            
        }   
        

        if (element.tagName == "SELECT") {
            currentValue = element.value;
            options = element.querySelectorAll("option");
            for (option of options) {
                if (option.value != element.value) {
                    option.classList.add("invisible");
                }
            }
        }
    }
}

function lowerCaseFirstLetter(string) {
    return string.charAt(0).toLowerCase() + string.slice(1);
}

function populateInformation() {

    //indicates whether a user is involved with active information requests
    returnBoolean = false;

    //booleans
    booleans = {
        openRequest1to0: false,

        openRequest2to0: false,
        openRequest2to1: false,

        openRequest3to0: false,
        openRequest3to1: false,
        openRequest3to2: false
    }

    //information section

    for (infoRequest of informationRequests) {
        
        const requesterRole = getUserRole(infoRequest.requestingParty.userID);
        const responderRole = getUserRole(infoRequest.respondingParty.userID);

        if (requesterRole <= userRole || responderRole <= userRole) {

            const requestTableBody = document.getElementById("requestTableBody");

            let tableRow = document.createElement("tr");

            let dateData = document.createElement("td");

            let reqParData = document.createElement("td");

            let reqBodData = document.createElement("td");

            let resParData = document.createElement("td");

            let resBodData = document.createElement("td");

            console.log(infoRequest);

            if (!infoRequest.responseBody) {

                if (request.status.statusID > requesterRole) {

                    resBodData.innerHTML = "<i>Requesting party approved without response.</i>";

                } else {

                    resBodData.innerHTML = "<i>Awaiting response...</i>";

                    booleans["openRequest" + requesterRole + "to" + responderRole] = true;
                }

            }

            for (property in infoRequest) {

                if (property != "request" && property != "informationRequestID") {

                    {
                        if (property == "respondingParty") {

                            let uRole = getUserRole(infoRequest[property].userID);

                            if (uRole == 0) {

                                resParData.innerHTML = "Reimbursement Request Creator";

                            } else if (uRole == 1) {

                                resParData.innerHTML = "Direct Supervisor";

                            } else if (uRole == 2) {

                                resParData.innerHTML = "Department Head";
                            }

                        } else if (property == "requestingParty") {

                            let uRole = getUserRole(infoRequest[property].userID);

                            if (uRole == 1) {

                                reqParData.innerHTML = "Direct Supervisor";

                            } else if (uRole == 2) {

                                reqParData.innerHTML = "Department Head";

                            } else if (uRole == 3) {

                                reqParData.innerHTML = "Benefits Coordinator";
                            }
                        
                        } else if (property == "requestDate") {

                            dateData.innerHTML = getDateString(infoRequest[property]);

                        } else  if (property == "responseBody"){

                            resBodData.innerHTML = infoRequest[property];

                        } else if (property == "requestBody"){

                            reqBodData.innerHTML = infoRequest[property];
                        }
                    }

                }
            }

            tableRow.appendChild(dateData);
            tableRow.appendChild(reqParData);
            tableRow.appendChild(reqBodData);
            tableRow.appendChild(resParData);
            tableRow.appendChild(resBodData);

            requestTableBody.appendChild(tableRow);
        }
    }

    //actions section

    if(request.status.statusID <= 3) {

        //set info request options by userrole
        for (let i = 0; i < 4; i++) {
            for (let j = i; j < 4; j++) {
                if (i != 0 && userRole == i) {

                    document.getElementById("informationActions").classList.remove("invisible");
                    document.getElementById("requestInformationCol").classList.remove("invisible");

                    requestOptionToHide = document.getElementById("requestInfoFrom" + j);
                    requestOptionToHide.classList.add("invisible");
                } 
            }
        }

        for (boolean in booleans) {
                
            requesterRole = boolean.charAt(11);
            responderRole = boolean.charAt(14);
                

            if(booleans[boolean] == true) {

                if (userRole == requesterRole) {

                    returnBoolean = true;

                    document.getElementById("requestInfoFrom" + responderRole).classList.add("invisible");

                } else if (userRole == responderRole) {

                    returnBoolean = true;

                    document.getElementById("informationActions").classList.remove("invisible");

                    document.getElementById("submitInformationCol").classList.remove("invisible");
                    
                    respondingPartyLabel = document.getElementById("sumbitInformationLabel");

                    let reqRoleString = "";

                    if (requesterRole == 1) {

                        reqRoleString = "Direct Supervisor";

                    } else if (requesterRole == 2) {

                        reqRoleString = "Department Head";

                    } else if (requesterRole == 3) {

                        reqRoleString = "Benefits Coordinator";

                    }

                    respondingPartyLabel.innerHTML = "Provide additional information for " + reqRoleString + ":";

                }
            }
        }

    }

    return returnBoolean;
}

function supportsLocalStorage() {
    try {
        return 'localStorage' in window && window['localStorage'] !== null;
    } catch (e) {
        return false;
    }
}

function getDateString(dateLong) {

    let date = new Date(dateLong);

    let year = date.getFullYear();

    let month = String(date.getMonth() + 1);
    if (month.length == 1) {
        
        month = "0" + month;
    }

    let day = String(date.getDate());
    if (day.length == 1) {

        day = "0" + day;
    }

    return (year + "-" + month + "-" + day);
}
