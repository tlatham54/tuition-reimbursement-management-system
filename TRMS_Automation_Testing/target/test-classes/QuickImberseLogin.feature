Feature: Logging into QuickImberse

  #will execute these steps before each scenario
  Background:
    Given The Guest is on the QuickImberse Login Page

  Scenario Outline: The Guest tries to login
    When The User enters her "<username>" and "<password>"
    And  The User clicks the Login button
    Then The User should be on the landing page

    Examples:
      |     username      |    password         |
      |     punkRocker99  |    password         |
      |     direfire      |    password         |
      |     whysomany8    |    password         |