package pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class QILogin {

    public WebDriver driver;


    //Fields that represent Elements in the HTML

    @FindBy(id = "usernameInput")
    public static WebElement usernameInput;

    @FindBy(id = "passwordInput")
    public static WebElement passwordInput;

    @FindBy(xpath = "/html/body/div/div/div/div/div/button")
    public static WebElement loginButton;



    public QILogin(WebDriver driver) {
        this.driver = driver;
        PageFactory.initElements(driver, this);
    }

}
