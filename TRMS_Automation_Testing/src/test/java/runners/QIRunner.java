package runners;

//This class is responsible for Running the Tests by pulling in the
//feature files and step implementations and executing them
//appropriately

import io.cucumber.junit.Cucumber;
import io.cucumber.junit.CucumberOptions;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.runner.RunWith;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import pages.QILogin;

import java.util.concurrent.TimeUnit;

@RunWith(Cucumber.class)
@CucumberOptions(features = "src/test/resources", glue = {"steps"})
public class QIRunner {

    public static WebDriver driver;
    public static QILogin qiLogin;

    @BeforeClass
    public static void setUp() {
        String path = "C:/Users/tlath/OneDrive/Documents/Revature/chromedriver.exe";
        System.setProperty("webdriver.chrome.driver", path);

        driver = new ChromeDriver();
        qiLogin = new QILogin(driver);
    }

    @AfterClass
    public static void tearDown() {
        driver.quit();
    }

}
