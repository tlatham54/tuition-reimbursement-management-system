package steps;



import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.junit.Assert;
import org.openqa.selenium.WebDriver;
import pages.QILogin;
import runners.QIRunner;

public class QILoginStepImpl {

    public static WebDriver driver = QIRunner.driver;

    @Given("The Guest is on the QuickImberse Login Page")
    public void the_guest_is_on_the_quick_imberse_login_page() {

        driver.get("file:///C:/Users/tlath/OneDrive/Documents/Revature/VisualStudio/TRMS/LoginPage.html");
    }

    @When("The User enters her {string} and {string}")
    public void theUserEntersHerAnd(String username, String password) {

        QILogin.usernameInput.sendKeys(username);
        QILogin.passwordInput.sendKeys(password);
    }

    @When("The User clicks the Login button")
    public void the_user_clicks_the_login_button() {

        QILogin.loginButton.click();
        try {
            Thread.sleep(100);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    @Then("The User should be on the landing page")
    public void the_user_should_be_on_the_landing_page() {

        Assert.assertEquals("QuickImberse Landing Page", driver.getTitle());
    }



}
