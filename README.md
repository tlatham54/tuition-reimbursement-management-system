# QuickImberse: Tuition Reimbursement Management System Project

## Description
This project is a fully functional tuition reimbursement management system that could be used by a company wanting to centralize and standardize the process of submitting and approving requests for a company-wide tuition reimbursement program. Features include:
- A login page (backend validation) that takes users to a personalized landing page after successful login.
- Options for different events that one might want reimbursed, such as college courses, certifications, seminars and more.
- A messaging system by which superiors can request more information about a reimbursement petition from subordinates, and subordinates can respond.
- A reimbursement limit of $1000 for each employee that resets on the anniversiery of the employee's start of employment.

## Technologies Used
- Java 1.8
- Javalin
- Apache Log4j
- Hibernate
- PostgreSQL
- Maven
- JUnit5
- Cucumber
- Selenium
- HTML/CSS/JavaScript

## Data Entities 
| Name | Description |
| ------ | ------ |
| Request | Holds information pertinent to the request, like who created it, when it was created and the corresponding event |
| Event | Contains details about an event, such as its date, time and location |
| User  | Keeps track of user information, like department, login credentials and who the user's direct supervisor is |
| Information Request  | Holds details about an information request, like who sent it, who received it and what the response is |


# Backend Endpoints

## Requests
- `POST /requests`: Create a new reimbursement request
    - **Required Fields:** 
        - `request`: The request being created
- `GET /requests`: Get a list of all requests
    - **Query Parameters**
        - `userID`: The ID of a user for retrieving all requests involving said user
- `GET /requests/:requestID`: Get a specific request
- `PUT /requests`: Update an existing request
    - **Required Fields:** 
        - `request`: The request being updated

### Object Fields
- `requestID`: Unique identifier
- `creator`: User who created the request
- `event`: Event for which the request was made


## Users
- `GET /users`: Get a list of all users
- `GET /users:userID` Get a specific user

### Object Fields
- `userID`: Unique identifier
- `Department`: User's department within the company
- `Supervisor`: User's supervisor (also a user)

## Information Requests
- `POST /informationRequests`: Create a new information request
    - **Required Fields:** 
        - `informationRequest`: Information request being created
- `GET /informationRequests`: Get all information requests
- `GET /informationRequests/:informationRequestID`: Get a specific information request
- `PUT /informationRequests`: Update an existing information request
    - **Required Fields:** 
        - `informationRequest`: Information request being updated

### Object Fields
- `informationRequestID`: Unique identifier
- `request`: Associated reimbursement request
- `requestingParty:` User requesting more information


