package com.revature.util;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

public class HibernateUtil {

    //abstract away creating our SessionFactory and the
    //process of creating Sessions from
    //that SessionFactory



    private static SessionFactory sf = new Configuration()
            .setProperty("hibernate.connection.url", System.getenv("DB_URL"))
            .setProperty("hibernate.connection.username", System.getenv("DB_USERNAME"))
            .setProperty("hibernate.connection.password", System.getenv("DB_PASSWORD"))
            .configure().buildSessionFactory();
    //Configuration object uses .configure() to establish configurations,
    //then it creates the SessionFactory

    public static Session getSession() {
        try{
            return sf.openSession();

        } catch (Throwable e) {
            System.out.println("Error in creating SessionFactory object."
                    + e.getMessage());
            throw new ExceptionInInitializerError(e);
        }
    }

}
