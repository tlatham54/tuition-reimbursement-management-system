package com.revature.controllers;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.revature.models.InformationRequest;
import com.revature.services.pushreqservices.InformationRequestServices;
import com.revature.util.MyLogger;
import io.javalin.http.Handler;

import java.util.List;

public class InformationRequestController {

    InformationRequestServices us;
    Gson gson =new GsonBuilder().create();

    public InformationRequestController(InformationRequestServices us) {
        this.us = us;
    }

    public Handler getInformationRequest = ctx -> {

        String idString = ctx.pathParam("informationRequestID");

        int informationRequestID = -1;

        try {
            informationRequestID = Integer.parseInt(idString);
        } catch (NumberFormatException e) {
            System.out.println((e.getMessage()));
        }

        InformationRequest informationRequest = us.getInformationRequest(informationRequestID);

        if(informationRequest == null) {
            ctx.status(404);
        } else {
            ctx.status(200);
            ctx.result(gson.toJson(informationRequest));
            ctx.contentType("application/json");
        }
    };

    public Handler getAllInformationRequests = ctx -> {

        List<InformationRequest> informationRequestList = us.getAllInformationRequests();
        ctx.result(gson.toJson(informationRequestList));
        ctx.contentType("application/json");
        ctx.status(200);

    };

    public Handler addInformationRequest = ctx -> {

        InformationRequest informationRequest = new InformationRequest();

        try {
            informationRequest = gson.fromJson(ctx.body(), InformationRequest.class);
            informationRequest = us.addInformationRequest(informationRequest);
            ctx.result(gson.toJson(informationRequest));
            ctx.status(201);
        } catch (Exception e) {
            MyLogger.logger.error(e.getMessage());
            ctx.status(400);
        }

    };

    public Handler updateInformationRequest = ctx -> {

        InformationRequest informationRequest = new InformationRequest();

        try {
            informationRequest = gson.fromJson(ctx.body(), InformationRequest.class);
            informationRequest.setInformationRequestID(Integer.parseInt(ctx.pathParam("informationRequestID")));

        } catch (Exception e) {
            MyLogger.logger.error(e.getMessage());
        }

        informationRequest = us.updateInformationRequest(informationRequest);

        if(informationRequest == null) {
            ctx.status(404);
        } else {
            ctx.result(gson.toJson(informationRequest));
            ctx.contentType("application/json");
            ctx.status(200);
        }
    };

}
