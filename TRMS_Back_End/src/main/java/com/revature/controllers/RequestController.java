package com.revature.controllers;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.revature.exceptions.EventAlreadyStartedException;
import com.revature.models.RequestFE;
import com.revature.services.mvpservices.RequestServices;
import com.revature.util.MyLogger;
import io.javalin.http.Handler;

import java.util.List;
import java.util.Optional;

public class RequestController {
    
    RequestServices rs;
    Gson gson = new GsonBuilder().create();

    public RequestController(RequestServices rs) {
        this.rs = rs;
    }

    public Handler getAllRequests = ctx -> {

        try {

            Optional<String> userIDQ = Optional.ofNullable(ctx.queryParam("userID"));

            if(userIDQ.isPresent()) {

                int userID = Integer.parseInt(userIDQ.get());

                List<RequestFE> requests = null;

                requests = rs.getAllRequests(userID);
                ctx.result(gson.toJson(requests));

                if (requests == null) {
                    ctx.status(404);
                } else {
                    ctx.status(200);
                }

            } else {
                List<RequestFE> userList = rs.getAllRequests();
                ctx.result(gson.toJson(userList));
                ctx.contentType("application/json");
                ctx.status(200);
            }

        } catch (Exception e) {
            MyLogger.logger.error(e.getMessage());
            e.printStackTrace();
            ctx.status(400);
        }
    };

    public Handler addNewRequest = ctx -> {

        RequestFE request = new RequestFE();

        try {

            request = gson.fromJson(ctx.body(), RequestFE.class);

            request = rs.addRequest(request);

            ctx.result(gson.toJson(request));

            ctx.status(201);

        } catch (EventAlreadyStartedException eas) {

            ctx.status(400);

        } catch (Exception e) {

            MyLogger.logger.error(e.getMessage());
            e.printStackTrace();
            ctx.status(500);
        }
    };

    public Handler getRequest = ctx -> {

        String idString = ctx.pathParam("requestID");

        int requestID = -1;

        try {
            requestID = Integer.parseInt(idString);
        } catch (NumberFormatException e) {
            System.out.println((e.getMessage()));
        }

        RequestFE request = rs.getRequest(requestID);

        if(request == null) {
            ctx.status(404);
        } else {
            ctx.status(200);
            ctx.result(gson.toJson(request));
            ctx.contentType("application/json");
        }
    };

    public Handler updateRequest = ctx -> {

        RequestFE requestFE = new RequestFE();

        try {
            requestFE = gson.fromJson(ctx.body(), RequestFE.class);
            requestFE = rs.updateRequest(requestFE);
        } catch (Exception e) {
            MyLogger.logger.error(e.getMessage());
            ctx.status(404);
            ctx.result("{}");
        }



        if(requestFE == null) {
            ctx.status(404);
        } else {
            ctx.result(gson.toJson(requestFE));
            ctx.contentType("application/json");
            ctx.status(200);
        }

    };


}

