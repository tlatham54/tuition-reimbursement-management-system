package com.revature.controllers;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.revature.models.UserFE;
import com.revature.services.mvpservices.UserServices;
import com.revature.util.MyLogger;
import io.javalin.http.Handler;

import java.util.List;
import java.util.Optional;

public class UserController {

    UserServices us;
    Gson gson = new GsonBuilder().create();

    public UserController(UserServices us) {
        this.us = us;
    }

    public Handler getUser = ctx -> {

        String idString = ctx.pathParam("userID");

        int userID = -1;

        try {
            userID = Integer.parseInt(idString);
        } catch (NumberFormatException e) {
            System.out.println((e.getMessage()));
        } catch (Exception e) {
            System.out.println((e.getMessage()));
        }

        UserFE user = us.getUser(userID);

        if(user == null) {
            ctx.status(404);
        } else {
            ctx.status(200);
            ctx.result(gson.toJson(user));
            ctx.contentType("application/json");
        }
    };

    public Handler getAllUsers = ctx -> {

        try {

            Optional<String> usernameQ = Optional.ofNullable(ctx.queryParam("username"));
            Optional<String> passwordQ = Optional.ofNullable(ctx.queryParam("password"));

            if(usernameQ.isPresent() && passwordQ.isPresent()) {

                String username = usernameQ.get();
                String password = passwordQ.get();
                UserFE ufe = null;

                ufe = us.attemptLogin(username, password);
                ctx.result(gson.toJson(ufe));

                if (ufe == null) {
                    ctx.status(404);
                } else {
                    ctx.status(200);
                }

            } else {
                List<UserFE> userList = us.getAllUsers();
                ctx.result(gson.toJson(userList));
                ctx.contentType("application/json");
                ctx.status(200);
            }

        } catch (Exception e) {
            MyLogger.logger.error(e.getMessage());
            e.printStackTrace();
            ctx.status(400);
        }
    };

}
