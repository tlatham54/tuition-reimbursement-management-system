package com.revature.services.pushreqservices;

import com.revature.models.InformationRequest;

import java.util.List;

public interface InformationRequestServices {

    public InformationRequest addInformationRequest(InformationRequest informationRequest);
    public List<InformationRequest> getAllInformationRequests();
    public InformationRequest getInformationRequest(int informationRequestID);
    public InformationRequest updateInformationRequest(InformationRequest updatedInformationRequest);
    public InformationRequest deleteInformationRequest(int informationRequestID);
}
