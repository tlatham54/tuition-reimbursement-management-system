package com.revature.services.pushreqservices;

import com.revature.daos.pushreqdaos.InformationRequestDao;
import com.revature.models.InformationRequest;
import com.revature.models.Request;
import com.revature.services.mvpservices.RequestServices;

import java.util.List;

public class InformationRequestServicesImpl implements InformationRequestServices {

    InformationRequestDao ird;

    public InformationRequestServicesImpl(InformationRequestDao ird) {
        this.ird = ird;
    }

    @Override
    public InformationRequest addInformationRequest(InformationRequest informationRequest) {

        return ird.addInformationRequest(informationRequest);
    }

    @Override
    public List<InformationRequest> getAllInformationRequests() {
        return ird.getAllInformationRequests();
    }

    @Override
    public InformationRequest getInformationRequest(int informationRequestID) {
        return ird.getInformationRequest(informationRequestID);
    }

    @Override
    public InformationRequest updateInformationRequest(InformationRequest updatedInformationRequest) {
        return ird.updateInformationRequest(updatedInformationRequest);
    }

    @Override
    public InformationRequest deleteInformationRequest(int informationRequestID) {
        return ird.deleteInformationRequest(informationRequestID);
    }
}
