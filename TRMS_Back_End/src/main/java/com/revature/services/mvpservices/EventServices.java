package com.revature.services.mvpservices;

import com.revature.models.Event;
import com.revature.models.EventFE;

import java.util.List;

public interface EventServices {

    public EventFE convertToEventFE(Event event);

}
