package com.revature.services.mvpservices;

import com.revature.models.Event;
import com.revature.models.EventFE;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

public class EventServicesImpl implements EventServices {

    public EventFE convertToEventFE(Event event) {

        EventFE efe = new EventFE(event);

        String[] dateAndTime = determineDateTimeStrings(event.getDateTime());

        efe.setDateString(dateAndTime[0]);
        efe.setTimeString(dateAndTime[1]);

        return efe;
    }

    private String[] determineDateTimeStrings(long dateTime) {

        String[] dateAndTime = new String[2];

        Date dt = new Date(dateTime);

        DateFormat justDate = new SimpleDateFormat("yyyy-MM-dd");
        DateFormat justTime = new SimpleDateFormat("HH:mm");

        dateAndTime[0] = justDate.format(dt);
        dateAndTime[1] = justTime.format(dt);

        return dateAndTime;
    }
}
