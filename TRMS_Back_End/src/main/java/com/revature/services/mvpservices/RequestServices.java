package com.revature.services.mvpservices;

import com.revature.models.Request;
import com.revature.models.RequestFE;


import java.util.List;

public interface RequestServices {

    public RequestFE addRequest(RequestFE rfe);
    public List<RequestFE> getAllRequests();
    public List<RequestFE> getAllRequests(int userID);
    public RequestFE getRequest(int requestID);
    public RequestFE updateRequest(RequestFE updatedRequest);
    public RequestFE convertToRequestFE(Request request);
    public double calculateProjectedReimbursement(RequestFE rfe);

}
