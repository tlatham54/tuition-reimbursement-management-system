package com.revature.services.mvpservices;

import com.revature.models.User;
import com.revature.models.UserFE;

import java.util.List;

public interface UserServices {

    public List<UserFE> getAllUsers();
    public UserFE getUser(int userID);
    public UserFE attemptLogin(String username, String password);

    public UserFE convertToUserFE(User user);
}
