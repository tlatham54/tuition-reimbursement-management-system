package com.revature.services.mvpservices;

import com.revature.daos.mvpdaos.DepartmentHeadDao;
import com.revature.daos.mvpdaos.RequestDao;
import com.revature.daos.mvpdaos.UserDao;
import com.revature.daos.pushreqdaos.InformationRequestDao;
import com.revature.exceptions.EventAlreadyStartedException;
import com.revature.models.*;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.concurrent.ThreadLocalRandom;

public class RequestServicesImpl implements RequestServices {

    RequestDao rd;
    InformationRequestDao ird;
    UserServices us;
    EventServices es;

    public RequestServicesImpl(RequestDao rd, InformationRequestDao ird, UserDao ud, DepartmentHeadDao dhd) {
        this.rd = rd;
        this.ird = ird;
        this.us = new UserServicesImpl(ud, dhd);
        this.es = new EventServicesImpl();
    }

    @Override
    public RequestFE addRequest(RequestFE rfe) throws EventAlreadyStartedException {

        //give it a tracking number
        rfe.setTrackingNumber(rfe.hashCode());

        //set dateTime created
        rfe.setDateCreated(System.currentTimeMillis());

        //convert date and time into single long
        String dateAndTime = rfe.getEventFE().getDateString() + "T" + rfe.getEventFE().getTimeString();

        DateFormat format = new SimpleDateFormat( "yyyy-MM-dd'T'HH:mm");

        Date dt = null;

        try {
            dt = format.parse(dateAndTime);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        long eventTime = dt.getTime();

        rfe.getEventFE().setDateTime(eventTime);

        //check date is not past current date
        //also check if it's urgent
        long halfAFortnight = (1000L * 60 * 60 * 24 * 7);
        long now = System.currentTimeMillis();

        if (eventTime - now <= 0) {

            throw new EventAlreadyStartedException();

        } else if (eventTime - now <= halfAFortnight) {

            rfe.setUrgent(true);
            rfe.getEvent().setDateTime(eventTime);

        } else {

            rfe.getEvent().setDateTime(eventTime);
        }

        //convert RequestFE into pure Request

        if (rfe.getStatus().getStatusID() == 3) {

            List<UserFE> users = us.getAllUsers();
            List<UserFE> benCos = new ArrayList<>();
            String benes = "Benefits";

            for (UserFE ufe : users) {
                if (benes.equals(ufe.getDepartment().getDepartmentName())
                        && ufe.getUserID() != rfe.getCreatorFE().getUserID()) {
                    benCos.add(ufe);
                }
            }
            int benCoID = ThreadLocalRandom.current().nextInt(0, benCos.size());

            rfe.setAssignedBC(benCos.get(benCoID));

        } else {

            rfe.setAssignedBC(null);
        }

        Request request = new Request(rfe);

        return convertToRequestFE(rd.addRequest(request));
    }

    @Override
    public List<RequestFE> getAllRequests() {

        List<Request> requests = rd.getAllRequests();
        List<RequestFE> requestsFE = new ArrayList<>();

        for (Request req : requests) {
            requestsFE.add(convertToRequestFE(req));
        }

        return requestsFE;
    }

    @Override
    public List<RequestFE> getAllRequests(int userID) {

        List<RequestFE> requests = getAllRequests();
        List<RequestFE> toRemove = new ArrayList<>();
        List<InformationRequest> infoRequests = ird.getAllInformationRequests();

        //remove all requests R s/t user doesn't have permission to view R
        for (RequestFE req : requests) {

            int creator =  req.getCreator().getUserID();
            int ds = req.getCreator().getSupervisor().getUserID();
            int dh = us.convertToUserFE(req.getCreator()).getDepartmentHead().getUserID();
            int bc = req.getAssignedBC().getUserID();
            int reqStatus = req.getStatus().getStatusID();
            boolean isPresentation = (req.getEvent().getGradingFormat().getType().equals("Presentation"));
            boolean gradeIsSubmitted = (req.getFgopFileName() != null);

            if (!infoRequests.isEmpty()) {

                for (InformationRequest irq : infoRequests) {

                    int responder = irq.getRespondingParty().getUserID();
                    int irReqID = irq.getRequest().getRequestID();
                    boolean needsResponse = (irq.getResponseBody() == null);

                    System.out.println(irq.getRequest().getRequestID());

                    if (
                            !( userID == creator


                            || (userID == ds && reqStatus == 1
                            || userID == ds && responder == ds && needsResponse
                            || userID == ds && reqStatus == 4 && isPresentation && gradeIsSubmitted)

                            || (userID == dh && reqStatus == 2
                            || userID == dh && responder == dh && needsResponse)

                            || (userID == bc && reqStatus == 3
                            || userID == bc && reqStatus == 4 && !isPresentation && gradeIsSubmitted)
                    )
                    ) {
                        System.out.println(req.getRequestID());
                        toRemove.add(req);
                    }
                }

            } else {

                if (
                    !(userID == creator

                    || (userID == ds && reqStatus == 1
                    || userID == ds && reqStatus == 4 && isPresentation && gradeIsSubmitted)

                    || (userID == dh && reqStatus == 2)

                    || (userID == bc && reqStatus == 3
                    || userID == bc && reqStatus == 4 && !isPresentation && gradeIsSubmitted)
                )
                ) {
                    toRemove.add(req);
                }
            }
        }

        requests.removeAll(toRemove);

        return requests;
    }

    @Override
    public RequestFE getRequest(int requestID) {

        return convertToRequestFE(rd.getRequest(requestID));
    }

    @Override
    public RequestFE updateRequest(RequestFE updatedRequest) {

        int reqStatus = updatedRequest.getStatus().getStatusID();

        if (reqStatus < 3) {

            updatedRequest.setAssignedBC(null);

        } else if (reqStatus == 3) {

            List<UserFE> users = us.getAllUsers();
            List<UserFE> benCos = new ArrayList<>();
            String benes = "Benefits";

            for (UserFE ufe : users) {
                if (benes.equals(ufe.getDepartment().getDepartmentName())
                    && ufe.getUserID() != updatedRequest.getCreatorFE().getUserID()) {

                    benCos.add(ufe);
                }
            }

            int benCoID = ThreadLocalRandom.current().nextInt(0, benCos.size());

            updatedRequest.setAssignedBC(benCos.get(benCoID));
        }

        Request updatedRequestRegular = new Request(updatedRequest);

        return convertToRequestFE(rd.updateRequest(updatedRequestRegular));
    }

    @Override
    public RequestFE convertToRequestFE(Request request) {

        RequestFE rfe = new RequestFE(request);

        rfe.setProjectedReimbursement(calculateProjectedReimbursement(rfe));
        rfe.setCreatorFE(us.convertToUserFE(request.getCreator()));
        rfe.setEventFE(es.convertToEventFE(request.getEvent()));

        if (rfe.getAssignedBC() == null) {
            rfe.setAssignedBC(new User());
        }

        return rfe;
    }

    @Override
    public double calculateProjectedReimbursement(RequestFE rfe) {
        //just need to find those pending/awarded Requests that were started within the current employee-year (year that
        //starts on the employee's start date)
        //so gonna need to get all requests with the same creator as this request AND are not denied

        double projectedReimbursement = 0;
        double amountLeft = 0;

        List<Request> creatorRequests = rd.getAllRequests();
        List<Request> toRemove = new ArrayList<>();

        creatorRequests.removeIf(creatorRequest ->                          //remove if:
                creatorRequest.getRequestID() == rfe.getRequestID() || //request is this request
                creatorRequest.getStatus().getStatusName().equals("Denied") || //request was denied
                creatorRequest.getCreator().getUserID() != rfe.getCreator().getUserID() //request wasn't made by same user
        );

        for (Request creatorRequest: creatorRequests) {
            long Y = 1000L * 60 * 60 * 24 * 365; //millis in a year
            long C = System.currentTimeMillis(); //current time
            long S = creatorRequest.getCreator().getStartDate(); //start date of employee

            long N = (C - S) / Y;
            //number of years between now and when the employee started (whole number, rounded down)

            if (creatorRequest.getDateCreated() < S + (Y * N)) {
                toRemove.add(creatorRequest);
            }
            //remove the request if it was created before the most recent anniversary of the employee's start date
        }

        creatorRequests.removeAll(toRemove);

        double pendingRequests = 0;
        double awardedRequests = 0;

        for (Request creatorRequest : creatorRequests) {

            if(creatorRequest.getStatus().getStatusName().equals("Reimbursed")) {

                awardedRequests += creatorRequest.getApprovedAmount();

            } else {

                pendingRequests += (creatorRequest.getEvent().getCost() *
                        creatorRequest.getEvent().getEventType().getReimbursementCoverage());
            }
        }

        //add the current request into the equation
        if (rfe.getStatus().getStatusID() >= 4) {
            projectedReimbursement = rfe.getApprovedAmount();
        } else {
            projectedReimbursement = rfe.getEvent().getEventType().getReimbursementCoverage() * rfe.getEvent().getCost();
            pendingRequests += projectedReimbursement;
            amountLeft = 1000 - pendingRequests - awardedRequests;

            if (projectedReimbursement > amountLeft) {
                projectedReimbursement = amountLeft;
            }

            if (projectedReimbursement < 0) {
                projectedReimbursement = 0;
            }
        }
        return Math.round(projectedReimbursement * 100.0) / 100.0;
    }

}
