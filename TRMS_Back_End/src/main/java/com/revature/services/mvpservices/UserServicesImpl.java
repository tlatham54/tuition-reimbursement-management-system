package com.revature.services.mvpservices;

import com.revature.daos.mvpdaos.DepartmentHeadDao;
import com.revature.daos.mvpdaos.UserDao;
import com.revature.models.*;

import java.util.ArrayList;
import java.util.List;

public class UserServicesImpl implements UserServices {

    UserDao ud;
    DepartmentHeadDao dhd;

    public UserServicesImpl(UserDao ud, DepartmentHeadDao dhd) {
        this.ud = ud;
        this.dhd = dhd;
    }

    @Override
    public List<UserFE> getAllUsers() {

        List<User> users = ud.getAllUsers();
        List<UserFE> usersFE = new ArrayList<>();

        for (User user : users) {
            usersFE.add(convertToUserFE(user));
        }

        return usersFE;

    }

    @Override
    public UserFE getUser(int userID) {
        return convertToUserFE(ud.getUser(userID));
    }

    @Override
    public UserFE attemptLogin(String username, String password) {

        UserFE returnUser = null;

        List<UserFE> users = getAllUsers();

        for (UserFE ufe : users) {
            if(username.equals(ufe.getUsername()) && password.equals(ufe.getPassword())) {

                returnUser = ufe;
            }
        }

        return returnUser;
    }

    @Override
    public UserFE convertToUserFE(User user) {

        UserFE ufe = new UserFE(user);

        ufe.setDepartmentHead(determineDepartmentHead(ufe));

        return ufe;
    }

    private User determineDepartmentHead(User ufe) {
        DepartmentHead dhEntry = dhd.getDepartmentHead(ufe.getDepartment().getDepartmentID());

        return dhEntry.getDepartmentHead();

    }
}
