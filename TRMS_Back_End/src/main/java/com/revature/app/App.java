package com.revature.app;

import com.revature.controllers.InformationRequestController;
import com.revature.controllers.RequestController;
import com.revature.controllers.UserController;
import com.revature.daos.mvpdaos.*;
import com.revature.daos.pushreqdaos.InformationRequestDao;
import com.revature.daos.pushreqdaos.InformationRequestDaoHBImpl;
import com.revature.services.mvpservices.RequestServices;
import com.revature.services.mvpservices.RequestServicesImpl;
import com.revature.services.mvpservices.UserServices;
import com.revature.services.mvpservices.UserServicesImpl;
import com.revature.services.pushreqservices.InformationRequestServices;
import com.revature.services.pushreqservices.InformationRequestServicesImpl;
import com.revature.util.HibernateUtil;
import io.javalin.Javalin;
import io.javalin.core.JavalinConfig;
import org.hibernate.Session;

public class App {

    public static void main(String[] args) {

        Javalin app = Javalin.create(JavalinConfig::enableCorsForAllOrigins);

        initHibernate();

        //Establish the Route/Endpoints that Javalin will manage.
        establishRoutes(app);

        //Run Javalin
        app.start(7000);
    }

    private static void initHibernate() {

        Session sesh = HibernateUtil.getSession();
        sesh.close();
    }

    private static void establishRoutes(Javalin app) {

        addUserRoutes(app);
        addInformationRequestRoutes(app);
        addRequestRoutes(app);

    }

    private static void addInformationRequestRoutes(Javalin app) {
        InformationRequestDao ird = new InformationRequestDaoHBImpl();
        InformationRequestServices irs = new InformationRequestServicesImpl(ird);
        InformationRequestController irc = new InformationRequestController(irs);

        app.get("/informationRequests/:informationRequestID", irc.getInformationRequest);
        app.get("/informationRequests", irc.getAllInformationRequests);
        app.post("/informationRequests", irc.addInformationRequest);
        app.put("/informationRequests/:informationRequestID", irc.updateInformationRequest);

    }

    private static void addUserRoutes(Javalin app) {
        
        UserDao ud = new UserDaoHBImpl();
        DepartmentDao dd = new DepartmentDaoHBImpl();
        DepartmentHeadDao dhd = new DepartmentHeadDaoHBImpl(dd);

        UserServices cs = new UserServicesImpl(ud, dhd);
        UserController cc = new UserController(cs);

        app.get("/users/:userID", cc.getUser);
        app.get("/users", cc.getAllUsers);
        
    }

    private static void addRequestRoutes(Javalin app) {

        RequestDao rd = new RequestDaoHBImpl();
        InformationRequestDao ird = new InformationRequestDaoHBImpl();
        UserDao ud = new UserDaoHBImpl();
        DepartmentDao dd = new DepartmentDaoHBImpl();
        DepartmentHeadDao dhd = new DepartmentHeadDaoHBImpl(dd);

        RequestServices rs = new RequestServicesImpl(rd, ird, ud, dhd);
        RequestController rc = new RequestController(rs);

        app.get("/requests", rc.getAllRequests);
        app.post("/requests", rc.addNewRequest);
        app.get("/requests/:requestID", rc.getRequest);
        app.put("/requests", rc.updateRequest);
    }

}
