package com.revature.daos.mvpdaos;

import com.revature.models.Department;
import com.revature.util.HibernateUtil;
import org.hibernate.HibernateException;
import org.hibernate.Session;

public class DepartmentDaoHBImpl implements DepartmentDao {

    @Override
    public Department getDepartment(int departmentID) {

        Session sesh = HibernateUtil.getSession();
        Department department = null;

        try{
            department = sesh.get(Department.class, departmentID);
        } catch (HibernateException e) {
            e.printStackTrace();
        } finally {
            sesh.close();
        }

        return department;
    }



}
