package com.revature.daos.mvpdaos;

import com.revature.models.Department;

import java.util.List;

public interface DepartmentDao {

    public Department getDepartment(int departmentID);

}
