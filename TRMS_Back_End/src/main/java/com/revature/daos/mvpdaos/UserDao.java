package com.revature.daos.mvpdaos;

import com.revature.models.User;

import java.util.List;

public interface UserDao {

    public List<User> getAllUsers();
    public User getUser(int userID);

}
