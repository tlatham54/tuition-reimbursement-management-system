package com.revature.daos.mvpdaos;

import com.revature.models.Request;

import java.util.List;

public interface RequestDao {

    public Request addRequest(Request request);
    public List<Request> getAllRequests();
    public Request getRequest(int requestID);
    public Request updateRequest(Request updatedRequest);

}
