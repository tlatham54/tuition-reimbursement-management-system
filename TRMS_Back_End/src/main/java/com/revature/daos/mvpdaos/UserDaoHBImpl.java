package com.revature.daos.mvpdaos;

import com.revature.models.User;
import com.revature.util.HibernateUtil;
import org.hibernate.HibernateException;
import org.hibernate.Session;

import java.util.List;

public class UserDaoHBImpl implements UserDao {

    @Override
    public List<User> getAllUsers() {

        Session sesh = HibernateUtil.getSession();
        List<User> users = null;

        try {
            users = sesh.createQuery("FROM User").list();
        } catch (HibernateException e) {
            e.printStackTrace();
        } finally {
            sesh.close();
        }

        return users;
    }

    @Override
    public User getUser(int userID) {

        Session sesh = HibernateUtil.getSession();
        User user = null;

        try{
            user = sesh.get(User.class, userID);
        } catch (HibernateException e) {
            e.printStackTrace();
        } finally {
            sesh.close();
        }

        return user;
    }


}
