package com.revature.daos.mvpdaos;

import com.revature.models.DepartmentHead;

import java.util.List;

public interface DepartmentHeadDao {


    public DepartmentHead getDepartmentHead(int departmentID);

}
