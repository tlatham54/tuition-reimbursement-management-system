package com.revature.daos.mvpdaos;

import com.revature.models.Request;
import com.revature.util.HibernateUtil;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.Transaction;

import java.util.List;

public class RequestDaoHBImpl implements RequestDao {


    @Override
    public Request addRequest(Request request) {

        System.out.println(request);

        Session sesh = HibernateUtil.getSession();

        try {

            sesh.beginTransaction();
            request.getEvent().setEventID((int)sesh.save(request.getEvent()));
            request.setRequestID((int)sesh.save(request));
            sesh.getTransaction().commit();

        } catch (HibernateException e) {

            e.printStackTrace();
            sesh.getTransaction().rollback();
            request = null;

        } finally {
            sesh.close();
        }

        return request;
    }

    @Override
    public List<Request> getAllRequests() {

        Session sesh = HibernateUtil.getSession();
        List<Request> requests = null;

        try {
            requests = sesh.createQuery("FROM Request").list();
        } catch (HibernateException e) {
            e.printStackTrace();
        } finally {
            sesh.close();
        }

        return requests;
    }

    @Override
    public Request getRequest(int requestID) {

        Session sesh = HibernateUtil.getSession();
        Request request = null;

        try{
            request = sesh.get(Request.class, requestID);
        } catch (HibernateException e) {
            e.printStackTrace();
        } finally {
            sesh.close();
        }

        return request;
    }

    @Override
    public Request updateRequest(Request updatedRequest) {

        Session sesh = HibernateUtil.getSession();
        Transaction tx = null;

        try {

            tx = sesh.beginTransaction();
            sesh.update(updatedRequest);
            tx.commit();

        } catch (HibernateException e) {

            e.printStackTrace();
            if (tx != null) {
                tx.rollback();
            }
            return null;
        } finally {
            sesh.close();
        }
        return updatedRequest;
    }

}
