package com.revature.daos.mvpdaos;

import com.revature.models.DepartmentHead;
import com.revature.util.HibernateUtil;
import org.hibernate.HibernateException;
import org.hibernate.Session;

public class DepartmentHeadDaoHBImpl implements DepartmentHeadDao {

    private DepartmentDao dd;

    public DepartmentHeadDaoHBImpl(DepartmentDao dd) {
        this.dd = dd;
    }

    @Override
    public DepartmentHead getDepartmentHead(int departmentID) {

        Session sesh = HibernateUtil.getSession();
        DepartmentHead departmentHead = null;

        try{
            departmentHead = sesh.get(DepartmentHead.class, departmentID);
        } catch (HibernateException e) {
            e.printStackTrace();
        } finally {
            sesh.close();
        }

        return departmentHead;
    }
}
