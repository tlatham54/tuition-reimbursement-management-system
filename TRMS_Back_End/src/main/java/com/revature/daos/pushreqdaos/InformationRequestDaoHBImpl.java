package com.revature.daos.pushreqdaos;

import com.revature.models.InformationRequest;
import com.revature.util.HibernateUtil;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.Transaction;

import java.util.List;

public class InformationRequestDaoHBImpl implements InformationRequestDao {

    @Override
    public InformationRequest addInformationRequest(InformationRequest informationRequest) {

        Session sesh = HibernateUtil.getSession();

        try {

            sesh.beginTransaction();
            informationRequest.setInformationRequestID((int)sesh.save(informationRequest));
            sesh.getTransaction().commit();

        } catch (HibernateException e) {

            e.printStackTrace();
            sesh.getTransaction().rollback();
            informationRequest = null;

        } finally {
            sesh.close();
        }

        return informationRequest;
    }

    @Override
    public List<InformationRequest> getAllInformationRequests() {

        Session sesh = HibernateUtil.getSession();
        List<InformationRequest> informationRequests = null;

        try {
            informationRequests = sesh.createQuery("FROM InformationRequest").list();
        } catch (HibernateException e) {
            e.printStackTrace();
        } finally {
            sesh.close();
        }

        return informationRequests;
    }

    @Override
    public InformationRequest getInformationRequest(int informationRequestID) {

        Session sesh = HibernateUtil.getSession();
        InformationRequest informationRequest = null;

        try{
            informationRequest = sesh.get(InformationRequest.class, informationRequestID);
        } catch (HibernateException e) {
            e.printStackTrace();
        } finally {
            sesh.close();
        }

        return informationRequest;
    }

    @Override
    public InformationRequest updateInformationRequest(InformationRequest updatedInformationRequest) {

        Session sesh = HibernateUtil.getSession();
        Transaction tx = null;

        try {

            tx = sesh.beginTransaction();
            sesh.update(updatedInformationRequest);
            tx.commit();

        } catch (HibernateException e) {

            e.printStackTrace();
            if (tx != null) {
                tx.rollback();
            }
            updatedInformationRequest = null;
        } finally {
            sesh.close();
        }
        return updatedInformationRequest;
    }

    @Override
    public InformationRequest deleteInformationRequest(int informationRequestID) {

        Session sesh = HibernateUtil.getSession();
        Transaction tx = null;
        InformationRequest returnInformationRequest = null;

        try {

            tx = sesh.beginTransaction();
            returnInformationRequest = sesh.get(InformationRequest.class, informationRequestID);
            sesh.delete(returnInformationRequest);
            tx.commit();

        } catch (HibernateException e) {
            e.printStackTrace();
            if(tx != null) {
                tx.rollback();
            }
            returnInformationRequest = null;
        } finally {
            sesh.close();
        }

        return returnInformationRequest;
    }
}
