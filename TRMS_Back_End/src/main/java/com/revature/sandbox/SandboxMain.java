package com.revature.sandbox;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

public class SandboxMain {

    public static void main(String[] args) {

        long dtLong = System.currentTimeMillis();
        Date dt = new Date(dtLong);

        DateFormat justDate = new SimpleDateFormat("yyyy-MM-dd");
        DateFormat justTime = new SimpleDateFormat("HH:mm");

        String date = justDate.format(dt);
        String time = justTime.format(dt);

        System.out.println(date);
        System.out.println(time);



    }
}
