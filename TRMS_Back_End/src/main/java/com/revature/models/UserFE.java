package com.revature.models;

import java.util.Objects;

public class UserFE extends User {

    private User departmentHead;

    public UserFE() {
    }

    public UserFE(User user) {
        super.setUserID(user.getUserID());
        super.setUsername(user.getUsername());
        super.setPassword(user.getPassword());
        super.setFirstName(user.getFirstName());
        super.setLastName(user.getLastName());
        super.setDepartment(user.getDepartment());
        super.setSupervisor(user.getSupervisor());
        super.setStartDate(user.getStartDate());
    }

    public User getDepartmentHead() {
        return departmentHead;
    }

    public void setDepartmentHead(User departmentHead) {
        this.departmentHead = departmentHead;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof UserFE)) return false;
        UserFE userFE = (UserFE) o;
        return getUserID() == userFE.getUserID() && getStartDate() == userFE.getStartDate() && Objects.equals(getUsername(), userFE.getUsername()) && Objects.equals(getPassword(), userFE.getPassword()) && Objects.equals(getFirstName(), userFE.getFirstName()) && Objects.equals(getLastName(), userFE.getLastName()) && Objects.equals(getDepartment(), userFE.getDepartment()) && Objects.equals(getSupervisor(), userFE.getSupervisor()) && Objects.equals(getDepartmentHead(), userFE.getDepartmentHead());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getUserID(), getUsername(), getPassword(), getFirstName(), getLastName(), getDepartment(), getSupervisor(), getStartDate(), getDepartmentHead());
    }

    @Override
    public String toString() {
        return "UserFE{" +
                super.toString() +
                "departmentHead=" + departmentHead +
                '}';
    }
}
