package com.revature.models;

import javax.persistence.*;
import java.util.Objects;

@Entity
@Table(name = "request_statuses")
public class RequestStatus {

    @Id
    @Column(name = "status_id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int statusID;

    @Column(name = "status_name")
    private String statusName;

    public RequestStatus() {
    }

    public RequestStatus(int statusID, String statusName) {
        this.statusID = statusID;
        this.statusName = statusName;
    }

    public int getStatusID() {
        return statusID;
    }

    public void setStatusID(int statusID) {
        this.statusID = statusID;
    }

    public String getStatusName() {
        return statusName;
    }

    public void setStatusName(String statusName) {
        this.statusName = statusName;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof RequestStatus)) return false;
        RequestStatus that = (RequestStatus) o;
        return getStatusID() == that.getStatusID() && Objects.equals(getStatusName(), that.getStatusName());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getStatusID(), getStatusName());
    }

    @Override
    public String toString() {
        return "RequestStatuses{" +
                "statusID=" + statusID +
                ", statusName='" + statusName + '\'' +
                '}';
    }
}
