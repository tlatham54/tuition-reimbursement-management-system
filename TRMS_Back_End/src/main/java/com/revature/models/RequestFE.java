package com.revature.models;

import java.util.Objects;

public class RequestFE extends Request {

    private double projectedReimbursement;
    private UserFE creatorFE;
    private EventFE eventFE;
    private String fgopString;

    public RequestFE() {
    }

    public RequestFE(Request request) {
        super.setRequestID(request.getRequestID());
        super.setCreator(request.getCreator());
        super.setEvent(request.getEvent());
        super.setFinalGradeOrPresentation(request.getFinalGradeOrPresentation());
        super.setFgopFileName(request.getFgopFileName());
        super.setAssignedBC(request.getAssignedBC());
        super.setApprovedAmount(request.getApprovedAmount());
        super.setStatus(request.getStatus());
        super.setDateCreated(request.getDateCreated());
        super.setDenialReason(request.getDenialReason());
        super.setFirstName(request.getFirstName());
        super.setLastName(request.getLastName());
        super.setDepartment(request.getDepartment());
        super.setJustification(request.getJustification());
        super.setUrgent(request.isUrgent());
        super.setAmountMoreThanAvailable(request.isAmountMoreThanAvailable());
        super.setTrackingNumber(request.getTrackingNumber());
    }

    public double getProjectedReimbursement() {
        return projectedReimbursement;
    }

    public void setProjectedReimbursement(double projectedReimbursement) {
        this.projectedReimbursement = projectedReimbursement;
    }

    public UserFE getCreatorFE() {
        return creatorFE;
    }

    public void setCreatorFE(UserFE creatorFE) {
        this.creatorFE = creatorFE;
    }

    public EventFE getEventFE() {
        return eventFE;
    }

    public void setEventFE(EventFE eventFE) {
        this.eventFE = eventFE;
    }

    public String getFgopString() {
        return fgopString;
    }

    public void setFgopString(String fgopString) {
        this.fgopString = fgopString;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof RequestFE)) return false;
        if (!super.equals(o)) return false;
        RequestFE requestFE = (RequestFE) o;
        return Double.compare(requestFE.getProjectedReimbursement(), getProjectedReimbursement()) == 0;
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), getProjectedReimbursement());
    }

    @Override
    public String toString() {
        return "RequestFE{" + super.toString() +
                "projectedReimbursement=" + projectedReimbursement +
                '}';
    }
}
