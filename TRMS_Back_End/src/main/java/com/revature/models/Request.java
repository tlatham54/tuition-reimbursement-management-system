package com.revature.models;

import org.hibernate.annotations.Type;

import javax.persistence.*;
import java.util.Arrays;
import java.util.Objects;

@Entity
@Table(name = "requests")
public class Request {

    @Id
    @Column(name = "request_id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int requestID;

    @ManyToOne
    @JoinColumn(name = "creator")
    private User creator; //fk

    @ManyToOne
    @JoinColumn(name = "event")
    private Event event; //fk

    @Lob
    @Type(type = "org.hibernate.type.BinaryType")
    @Column(name = "final_grade_or_presentation")
    private byte[] finalGradeOrPresentation;

    @Column(name = "fgop_file_name")
    private String fgopFileName;

    @ManyToOne
    @JoinColumn(name = "assigned_bc")
    private User assignedBC; //fk

    @Column(name = "approved_amount",
            columnDefinition = "numeric (7,2)")
    private double approvedAmount;

    @ManyToOne
    @JoinColumn(name = "status")
    private RequestStatus status; //fk

    @Column(name = "date_created")
    private long dateCreated;

    @Column(name = "denial_reason")
    private String denialReason;

    @Column(name = "first_name")
    private String firstName;

    @Column(name = "last_name")
    private String lastName;

    private String department;

    private String justification;

    @Column(columnDefinition = "boolean")
    private boolean urgent;

    @Column(name ="amount_more_than_available", columnDefinition = "boolean")
    private boolean amountMoreThanAvailable;

    @Column(name = "tracking_number")
    private int trackingNumber;




    public Request() {
    }

    public Request(int requestID, User creator, Event event, byte[] finalGradeOrPresentation, String fgopFileName, User assignedBC, double approvedAmount, RequestStatus status, long dateCreated, String denialReason, String firstName, String lastName, String department, String justification, boolean urgent, boolean amountMoreThanAvailable, int trackingNumber) {
        this.requestID = requestID;
        this.creator = creator;
        this.event = event;
        this.finalGradeOrPresentation = finalGradeOrPresentation;
        this.fgopFileName = fgopFileName;
        this.assignedBC = assignedBC;
        this.approvedAmount = approvedAmount;
        this.status = status;
        this.dateCreated = dateCreated;
        this.denialReason = denialReason;
        this.firstName = firstName;
        this.lastName = lastName;
        this.department = department;
        this.justification = justification;
        this.urgent = urgent;
        this.amountMoreThanAvailable = amountMoreThanAvailable;
        this.trackingNumber = trackingNumber;
    }

    public Request(RequestFE requestFE) {
        this.requestID = requestFE.getRequestID();
        this.creator = new User(requestFE.getCreatorFE());
        this.event = new Event(requestFE.getEventFE());
        this.finalGradeOrPresentation = requestFE.getFinalGradeOrPresentation();
        this.fgopFileName = requestFE.getFgopFileName();
        this.assignedBC = requestFE.getAssignedBC();
        this.approvedAmount = requestFE.getApprovedAmount();
        this.status = requestFE.getStatus();
        this.dateCreated = requestFE.getDateCreated();
        this.denialReason = requestFE.getDenialReason();
        this.firstName = requestFE.getFirstName();
        this.lastName = requestFE.getLastName();
        this.department = requestFE.getDepartment();
        this.justification = requestFE.getJustification();
        this.urgent = requestFE.isUrgent();
        this.amountMoreThanAvailable = requestFE.isAmountMoreThanAvailable();
        this.trackingNumber = requestFE.getTrackingNumber();
    }

    public int getRequestID() {
        return requestID;
    }

    public void setRequestID(int requestID) {
        this.requestID = requestID;
    }

    public User getCreator() {
        return creator;
    }

    public void setCreator(User creator) {
        this.creator = creator;
    }

    public Event getEvent() {
        return event;
    }

    public void setEvent(Event event) {
        this.event = event;
    }

    public byte[] getFinalGradeOrPresentation() {
        return finalGradeOrPresentation;
    }

    public void setFinalGradeOrPresentation(byte[] finalGradeOrPresentation) {
        this.finalGradeOrPresentation = finalGradeOrPresentation;
    }

    public String getFgopFileName() {
        return fgopFileName;
    }

    public void setFgopFileName(String fgopFileName) {
        this.fgopFileName = fgopFileName;
    }

    public User getAssignedBC() {
        return assignedBC;
    }

    public void setAssignedBC(User assignedBC) {
        this.assignedBC = assignedBC;
    }

    public double getApprovedAmount() {
        return approvedAmount;
    }

    public void setApprovedAmount(double approvedAmount) {
        this.approvedAmount = approvedAmount;
    }

    public RequestStatus getStatus() {
        return status;
    }

    public void setStatus(RequestStatus status) {
        this.status = status;
    }

    public long getDateCreated() {
        return dateCreated;
    }

    public void setDateCreated(long dateCreated) {
        this.dateCreated = dateCreated;
    }

    public String getDenialReason() {
        return denialReason;
    }

    public void setDenialReason(String denialReason) {
        this.denialReason = denialReason;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getDepartment() {
        return department;
    }

    public void setDepartment(String department) {
        this.department = department;
    }

    public String getJustification() {
        return justification;
    }

    public void setJustification(String justification) {
        this.justification = justification;
    }

    public boolean isUrgent() {
        return urgent;
    }

    public void setUrgent(boolean urgent) {
        this.urgent = urgent;
    }

    public boolean isAmountMoreThanAvailable() {
        return amountMoreThanAvailable;
    }

    public void setAmountMoreThanAvailable(boolean amountMoreThanAvailable) {
        this.amountMoreThanAvailable = amountMoreThanAvailable;
    }

    public int getTrackingNumber() {
        return trackingNumber;
    }

    public void setTrackingNumber(int trackingNumber) {
        this.trackingNumber = trackingNumber;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Request)) return false;
        Request request = (Request) o;
        return getRequestID() == request.getRequestID() && Double.compare(request.getApprovedAmount(), getApprovedAmount()) == 0 && isUrgent() == request.isUrgent() && isAmountMoreThanAvailable() == request.isAmountMoreThanAvailable() && Objects.equals(getCreator(), request.getCreator()) && Objects.equals(getEvent(), request.getEvent()) && Arrays.equals(getFinalGradeOrPresentation(), request.getFinalGradeOrPresentation()) && Objects.equals(getFgopFileName(), request.getFgopFileName()) && Objects.equals(getAssignedBC(), request.getAssignedBC()) && Objects.equals(getStatus(), request.getStatus()) && Objects.equals(getDenialReason(), request.getDenialReason()) && Objects.equals(getFirstName(), request.getFirstName()) && Objects.equals(getLastName(), request.getLastName()) && Objects.equals(getDepartment(), request.getDepartment()) && Objects.equals(getJustification(), request.getJustification());
    }

    @Override
    public int hashCode() {
        int result = Objects.hash(getRequestID(), getCreator(), getEvent(), getFgopFileName(), getAssignedBC(), getApprovedAmount(), getStatus(), getDenialReason(), getFirstName(), getLastName(), getDepartment(), getJustification(), isUrgent(), isAmountMoreThanAvailable());
        result = 31 * result + Arrays.hashCode(getFinalGradeOrPresentation());
        return result;
    }

    @Override
    public String toString() {
        return "Request{" +
                "requestID=" + requestID +
                ", creator=" + creator +
                ", event=" + event +
                ", fgopFileName='" + fgopFileName + '\'' +
                ", assignedBC=" + assignedBC +
                ", approvedAmount=" + approvedAmount +
                ", status=" + status +
                ", dateCreated=" + dateCreated +
                ", denialReason='" + denialReason + '\'' +
                ", firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", department='" + department + '\'' +
                ", justification='" + justification + '\'' +
                ", urgent=" + urgent +
                ", amountMoreThanAvailable=" + amountMoreThanAvailable +
                ", trackingNumber=" + trackingNumber +
                '}';
    }


}
