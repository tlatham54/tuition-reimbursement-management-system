package com.revature.models;

import javax.persistence.*;
import java.util.Objects;

@Entity
@Table(name = "event_types")
public class EventType {

    @Id
    @Column(name = "event_type_id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int eventTypeID;

    @Column(name = "type_name")
    private String typeName;

    @Column(name = "reimbursement_coverage",
    columnDefinition = "numeric (3,2)")
    private double reimbursementCoverage;

    public EventType() {
    }

    public EventType(int eventTypeID, String typeName, double reimbursementCoverage) {
        this.eventTypeID = eventTypeID;
        this.typeName = typeName;
        this.reimbursementCoverage = reimbursementCoverage;
    }

    public int getEventTypeID() {
        return eventTypeID;
    }

    public void setEventTypeID(int eventTypeID) {
        this.eventTypeID = eventTypeID;
    }

    public String getTypeName() {
        return typeName;
    }

    public void setTypeName(String typeName) {
        this.typeName = typeName;
    }

    public double getReimbursementCoverage() {
        return reimbursementCoverage;
    }

    public void setReimbursementCoverage(double reimbursementCoverage) {
        this.reimbursementCoverage = reimbursementCoverage;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof EventType)) return false;
        EventType eventType = (EventType) o;
        return getEventTypeID() == eventType.getEventTypeID() && Double.compare(eventType.getReimbursementCoverage(), getReimbursementCoverage()) == 0 && Objects.equals(getTypeName(), eventType.getTypeName());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getEventTypeID(), getTypeName(), getReimbursementCoverage());
    }

    @Override
    public String toString() {
        return "EventType{" +
                "eventTypeID=" + eventTypeID +
                ", typeName='" + typeName + '\'' +
                ", reimbursementCoverage=" + reimbursementCoverage +
                '}';
    }
}
