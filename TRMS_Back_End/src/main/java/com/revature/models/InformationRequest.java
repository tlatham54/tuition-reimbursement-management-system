package com.revature.models;

import javax.persistence.*;
import java.util.Objects;

@Entity
@Table(name = "information_requests")
public class InformationRequest {

    @Id
    @Column(name = "information_request_id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int informationRequestID;

    @ManyToOne
    @JoinColumn(name = "request_id")
    private Request request; //fk

    @ManyToOne
    @JoinColumn(name = "requesting_party")
    private User requestingParty; //fk

    @ManyToOne
    @JoinColumn(name = "responding_party")
    private User respondingParty; //fk

    @Column(name = "request_body")
    private String requestBody;

    @Column(name = "response_body")
    private String responseBody;

    @Column(name = "request_date")
    private long requestDate;

    public InformationRequest() {
    }

    public InformationRequest(int informationRequestID, Request requestID, User requestingParty, User respondingParty, String requestBody, String responseBody, long requestDate) {
        this.informationRequestID = informationRequestID;
        this.request = requestID;
        this.requestingParty = requestingParty;
        this.respondingParty = respondingParty;
        this.requestBody = requestBody;
        this.responseBody = responseBody;
        this.requestDate = requestDate;
    }

    public int getInformationRequestID() {
        return informationRequestID;
    }

    public void setInformationRequestID(int informationRequestID) {
        this.informationRequestID = informationRequestID;
    }

    public Request getRequest() {
        return request;
    }

    public void setRequest(Request request) {
        this.request = request;
    }

    public User getRequestingParty() {
        return requestingParty;
    }

    public void setRequestingParty(User requestingParty) {
        this.requestingParty = requestingParty;
    }

    public User getRespondingParty() {
        return respondingParty;
    }

    public void setRespondingParty(User respondingParty) {
        this.respondingParty = respondingParty;
    }

    public String getRequestBody() {
        return requestBody;
    }

    public void setRequestBody(String requestBody) {
        this.requestBody = requestBody;
    }

    public String getResponseBody() {
        return responseBody;
    }

    public void setResponseBody(String responseBody) {
        this.responseBody = responseBody;
    }

    public long getRequestDate() {
        return requestDate;
    }

    public void setRequestDate(long requestDate) {
        this.requestDate = requestDate;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof InformationRequest)) return false;
        InformationRequest that = (InformationRequest) o;
        return getInformationRequestID() == that.getInformationRequestID() && getRequest() == that.getRequest() && getRequestingParty() == that.getRequestingParty() && getRespondingParty() == that.getRespondingParty() && getRequestDate() == that.getRequestDate() && Objects.equals(getRequestBody(), that.getRequestBody()) && Objects.equals(getResponseBody(), that.getResponseBody());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getInformationRequestID(), getRequest(), getRequestingParty(), getRespondingParty(), getRequestBody(), getResponseBody(), getRequestDate());
    }

    @Override
    public String toString() {
        return "InformationRequest{" +
                "informationRequestID=" + informationRequestID +
                ", requestID=" + request +
                ", requestingParty=" + requestingParty +
                ", respondingParty=" + respondingParty +
                ", requestBody='" + requestBody + '\'' +
                ", responseBody='" + responseBody + '\'' +
                ", requestDate=" + requestDate +
                '}';
    }
}
