package com.revature.models;

import javax.persistence.*;
import java.util.Objects;

@Entity
@Table(name = "events")
public class Event {

    @Id
    @Column(name = "event_id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int eventID;

    @ManyToOne
    @JoinColumn(name = "event_type")
    private EventType eventType; //fk

    @Column(name = "date_time")
    private long dateTime;

    private String location;

    @Column(columnDefinition = "numeric (7,2)")
    private double cost;

    private String description;

    @ManyToOne
    @JoinColumn(name = "grading_format")
    private GradingFormat gradingFormat; //fk

    @Column(name = "passing_grade")
    private String passingGrade;

    @Column(name = "missed_work")
    private String missedWork;





    public Event() {
    }

    public Event(int eventID, EventType eventType, long dateTime, String location, double cost, String description, GradingFormat gradingFormat, String passingGrade, String missedWork) {
        this.eventID = eventID;
        this.eventType = eventType;
        this.dateTime = dateTime;
        this.location = location;
        this.cost = cost;
        this.description = description;
        this.gradingFormat = gradingFormat;
        this.passingGrade = passingGrade;
        this.missedWork = missedWork;
    }

    public Event(EventFE efe) {
        this.eventID = efe.getEventID();
        this.eventType = efe.getEventType();
        this.dateTime = efe.getDateTime();
        this.location = efe.getLocation();
        this.cost = efe.getCost();
        this.description = efe.getDescription();
        this.gradingFormat = efe.getGradingFormat();
        this.passingGrade = efe.getPassingGrade();
        this.missedWork = efe.getMissedWork();
    }




    public int getEventID() {
        return eventID;
    }

    public void setEventID(int eventID) {
        this.eventID = eventID;
    }

    public EventType getEventType() {
        return eventType;
    }

    public void setEventType(EventType eventType) {
        this.eventType = eventType;
    }

    public long getDateTime() {
        return dateTime;
    }

    public void setDateTime(long dateTime) {
        this.dateTime = dateTime;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public double getCost() {
        return cost;
    }

    public void setCost(double cost) {
        this.cost = cost;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public GradingFormat getGradingFormat() {
        return gradingFormat;
    }

    public void setGradingFormat(GradingFormat gradingFormat) {
        this.gradingFormat = gradingFormat;
    }

    public String getPassingGrade() {
        return passingGrade;
    }

    public void setPassingGrade(String passingGrade) {
        this.passingGrade = passingGrade;
    }

    public String getMissedWork() {
        return missedWork;
    }

    public void setMissedWork(String missedWork) {
        this.missedWork = missedWork;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Event)) return false;
        Event event = (Event) o;
        return getEventID() == event.getEventID() && getDateTime() == event.getDateTime() && Double.compare(event.getCost(), getCost()) == 0 && Objects.equals(getEventType(), event.getEventType()) && Objects.equals(getLocation(), event.getLocation()) && Objects.equals(getDescription(), event.getDescription()) && Objects.equals(getGradingFormat(), event.getGradingFormat()) && Objects.equals(getPassingGrade(), event.getPassingGrade()) && Objects.equals(getMissedWork(), event.getMissedWork());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getEventID(), getEventType(), getDateTime(), getLocation(), getCost(), getDescription(), getGradingFormat(), getPassingGrade(), getMissedWork());
    }

    @Override
    public String toString() {
        return "Event{" +
                "eventID=" + eventID +
                ", eventType=" + eventType +
                ", dateTime=" + dateTime +
                ", location='" + location + '\'' +
                ", cost=" + cost +
                ", description='" + description + '\'' +
                ", gradingFormat=" + gradingFormat +
                ", passingGrade='" + passingGrade + '\'' +
                ", missedWork='" + missedWork + '\'' +
                '}';
    }
}
