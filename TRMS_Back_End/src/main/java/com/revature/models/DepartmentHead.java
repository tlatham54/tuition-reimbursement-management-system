package com.revature.models;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Objects;

@Entity
@Table(name = "department_heads")
public class DepartmentHead implements Serializable {

    @Id
    @OneToOne
    @JoinColumn(name = "department_id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Department department;

    @OneToOne
    @JoinColumn(name = "department_head_id")
    private User departmentHead;

    public DepartmentHead() {
    }

    public DepartmentHead(Department department, User departmentHead) {
        this.department = department;
        this.departmentHead = departmentHead;
    }

    public Department getDepartment() {
        return department;
    }

    public void setDepartment(Department department) {
        this.department = department;
    }

    public User getDepartmentHead() {
        return departmentHead;
    }

    public void setDepartmentHead(User departmentHead) {
        this.departmentHead = departmentHead;
    }

    @Override
    public String toString() {
        return "DepartmentHead{" +
                "departmentID=" + department +
                ", departmentHeadID=" + departmentHead +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof DepartmentHead)) return false;
        DepartmentHead that = (DepartmentHead) o;
        return Objects.equals(getDepartment(), that.getDepartment()) && Objects.equals(getDepartmentHead(), that.getDepartmentHead());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getDepartment(), getDepartmentHead());
    }
}
