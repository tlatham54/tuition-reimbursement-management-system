package com.revature.models;

import javax.persistence.*;
import java.util.Objects;

@Entity
@Table(name = "grading_formats")
public class GradingFormat {

    @Id
    @Column(name = "format_id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int formatID;

    @Column(name = "format_type",
    columnDefinition = "varchar (50) check (format_type in ('GPA', 'Letter Grade', 'Percentage', 'Pass/Fail', 'Presentation'))")
    private String type;




    public GradingFormat() {
    }

    public GradingFormat(int formatID, String type) {
        this.formatID = formatID;
        this.type = type;
    }

    public int getFormatID() {
        return formatID;
    }

    public void setFormatID(int formatID) {
        this.formatID = formatID;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof GradingFormat)) return false;
        GradingFormat that = (GradingFormat) o;
        return getFormatID() == that.getFormatID() && Objects.equals(getType(), that.getType());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getFormatID(), getType());
    }

    @Override
    public String toString() {
        return "GradingFormat{" +
                "formatID=" + formatID +
                ", type='" + type + '\'' +
                '}';
    }
}
