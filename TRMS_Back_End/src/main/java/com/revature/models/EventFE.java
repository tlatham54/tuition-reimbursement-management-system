package com.revature.models;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Objects;

public class EventFE extends Event {

    private String dateString;
    private String timeString;

    public EventFE(Event event) {
        super(
            event.getEventID(),
            event.getEventType(),
            event.getDateTime(),
            event.getLocation(),
            event.getCost(),
            event.getDescription(),
            event.getGradingFormat(),
            event.getPassingGrade(),
            event.getMissedWork()
        );
    }

    public EventFE() {
    }

    public EventFE(String dateString, String timeString) {
        this.dateString = dateString;
        this.timeString = timeString;
    }

    public String getDateString() {
        return dateString;
    }

    public void setDateString(String dateString) {
        this.dateString = dateString;
    }

    public String getTimeString() {
        return timeString;
    }

    public void setTimeString(String timeString) {
        this.timeString = timeString;
    }


    @Override
    public String toString() {
        return "EventFE{" +
                "dateString='" + dateString + '\'' +
                ", timeString='" + timeString + '\'' +
                '}';
    }
}
