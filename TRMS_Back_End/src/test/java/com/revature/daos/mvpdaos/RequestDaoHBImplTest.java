package com.revature.daos.mvpdaos;

import com.ninja_squad.dbsetup.*;
import com.revature.models.*;
import com.revature.util.HibernateUtil;
import org.hibernate.HibernateException;
import org.hibernate.SQLQuery;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.query.NativeQuery;
import org.hibernate.query.Query;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;


import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class RequestDaoHBImplTest {


    RequestDao rd = new RequestDaoHBImpl();

    Department ops = new Department(1, "Operations");
    Department benes = new Department(4, "Benefits");
    Department execs = new Department(5, "Executives");

    //DH's ds
    User dsOfDh = new User(0, "username", "password", "firstName", "lastName", execs, null, 0);

    //ops DH
    User dh = new User(0, "username", "password", "firstName", "lastName", ops, dsOfDh, 0);

    //u1's supervisor
    User ds = new User(0, "username", "password", "firstName", "lastName", ops, dh, 0);

    //ops employee
    User u = new User(0, "username", "password", "firstName", "lastName", ops, ds, 0);

    //benco dh
    User bcDh = new User(0, "username", "password", "firstName", "lastName", benes, dsOfDh, 0);

    //benco ds
    User bcDs = new User(0, "username", "password", "firstName", "lastName", benes, bcDh, 0);

    //benco
    User bc = new User(0, "username", "password", "firstName", "lastName", benes, bcDs, 0);

    DepartmentHead dhOps = new DepartmentHead(ops, dh);
    DepartmentHead dhBenes = new DepartmentHead(benes, bcDh);

    EventType college = new EventType(1, "University Course", 0.80);
    GradingFormat GPA = new GradingFormat(1, "GPA");

    Event e1 = new Event(0, college, 10000000, "location" , 500, "description", GPA, "passingGrade", "missedWork");
    Event e2 = new Event(0, college, 10000000, "location" , 500, "description", GPA, "passingGrade", "missedWork");
    Event e3 = new Event(0, college, 10000000, "location" , 500, "description", GPA, "passingGrade", "missedWork");

    RequestStatus created = new RequestStatus(1, "Created");

    Request r1 = new Request(0, u, e1, null, "fgopFileName", null, 0, created, 100000, "denialReason", "firstName", "lastName", "department", "justification", false, false, 1);
    Request r2 = new Request(0, ds, e2, null, "fgopFileName", null, 0, created, 100000, "denialReason", "firstName", "lastName", "department", "justification", false, false, 1);
    Request r3 = new Request(0, dh, e3, null, "fgopFileName", null, 0, created, 100000, "denialReason", "firstName", "lastName", "department", "justification", false, false, 1);
    Request r4 = new Request(0, bc, e3, null, "fgopFileName", null, 0, created, 100000, "denialReason", "firstName", "lastName", "department", "justification", false, false, 1);

    @BeforeEach
    void initialize() {

        dsOfDh.setSupervisor(dsOfDh);

        //Drop all tables, create them, initialize the reference tables
        //Add all the init data
        Session sesh = HibernateUtil.getSession();
        Transaction tx = null;
        try {
            tx = sesh.beginTransaction();
            NativeQuery query;
            {query = sesh.createSQLQuery("--DROPPERS--\n" +
                    "\t\tdrop table if exists information_requests;\n" +
                    "\tdrop table if exists requests;\n" +
                    "drop table if exists request_statuses;\n" +
                    "\n" +
                    "\tdrop table if exists events;\n" +
                    "drop table if exists grading_formats;\n" +
                    "drop table if exists event_types;\n" +
                    "\n" +
                    "\t\tdrop table if exists department_heads;\n" +
                    "\tdrop table if exists users;\n" +
                    "drop table if exists departments;\n" +
                    "\n" +
                    "-----------------------------------------------\n" +
                    "--CREATORS--\n" +
                    "create table if not exists departments (\n" +
                    "\tdepartment_id SERIAL primary key,\n" +
                    "\tdepartment_name VARCHAR (50)\n" +
                    ");\n" +
                    "\n" +
                    "create table if not exists users (\n" +
                    "\tuser_id SERIAL primary key,\n" +
                    "\tusername varchar (20),\n" +
                    "\tpassword varchar (20),\n" +
                    "\tfirst_name VARCHAR (50),\n" +
                    "\tlast_name VARCHAR (50),\n" +
                    "\tdepartment int references departments(department_id),\n" +
                    "\tsupervisor int references users(user_id),\n" +
                    "\tstart_date bigint\n" +
                    ");\n" +
                    "\n" +
                    "create table if not exists department_heads (\n" +
                    "\tdepartment_id serial primary key references departments(department_id),\n" +
                    "\tdepartment_head_id int references users(user_id)\n" +
                    ");\n" +
                    "\t\n" +
                    "\n" +
                    "create table if not exists grading_formats (\n" +
                    "\tformat_id SERIAL primary key,\n" +
                    "\tformat_type varchar (50) check (format_type in ('GPA', 'Letter Grade', 'Percentage', 'Pass/Fail', 'Presentation'))\n" +
                    ");\n" +
                    "\n" +
                    "create table if not exists event_types (\n" +
                    "\tevent_type_id serial primary key,\n" +
                    "\ttype_name varchar (50),\n" +
                    "\treimbursement_coverage numeric (3,2)\n" +
                    ");\n" +
                    "\n" +
                    "create table if not exists events (\n" +
                    "\tevent_id serial primary key,\n" +
                    "\tevent_type int references event_types(event_type_id),\n" +
                    "\tdate_time bigint,\n" +
                    "\tlocation varchar (50),\n" +
                    "\tcost numeric (7,2),\n" +
                    "\tdescription varchar (100),\n" +
                    "\tgrading_format int references grading_formats(format_id),\n" +
                    "\tpassing_grade varchar (50),\n" +
                    "\tmissed_work varchar (500)\n" +
                    ");\n" +
                    "\n" +
                    "create table if not exists request_statuses (\n" +
                    "\tstatus_id serial primary key,\n" +
                    "\tstatus_name varchar (50)\n" +
                    ");\n" +
                    "\n" +
                    "create table if not exists requests (\n" +
                    "\trequest_id serial primary key,\n" +
                    "\tcreator int references users(user_id),\n" +
                    "\tevent int references events(event_id),\n" +
                    "\tfinal_grade_or_presentation bytea,\n" +
                    "\tfgop_file_name varchar(100),\n" +
                    "\tassigned_bc int references users(user_id),\n" +
                    "\tapproved_amount numeric (7,2),\n" +
                    "\tstatus int references request_statuses(status_id),\n" +
                    "\t\n" +
                    "\tdate_created bigint,\n" +
                    "\tdenial_reason varchar(500),\n" +
                    "\t\n" +
                    "\tfirst_name varchar(50),\n" +
                    "\tlast_name varchar(50),\n" +
                    "\tdepartment varchar(50),\n" +
                    "\t\n" +
                    "\tjustification varchar (50),\n" +
                    "\turgent boolean,\n" +
                    "\tamount_more_than_available boolean,\n" +
                    "\ttracking_number int\n" +
                    ");\n" +
                    "\n" +
                    "create table if not exists information_requests (\n" +
                    "\tinformation_request_id serial primary key,\n" +
                    "\trequest_id int references requests(request_id),\n" +
                    "\trequesting_party int references users(user_id),\n" +
                    "\tresponding_party int references users(user_id),\n" +
                    "\trequest_body varchar (1000),\n" +
                    "\tresponse_body varchar (1000),\n" +
                    "\trequest_date bigint\n" +
                    ");\n" +
                    "\n" +
                    "-----------------------------------------------\n" +
                    "--POPULATORS--\n" +
                    "\t--REFERENCE TABLES--\n" +
                    "\n" +
                    "insert into event_types values\n" +
                    "(default, 'University Course', 0.80),\n" +
                    "(default, 'Seminar', 0.60),\n" +
                    "(default, 'Certification Preparation Class', 0.75),\n" +
                    "(default, 'Certification', 1.00),\n" +
                    "(default, 'Technical Training', 0.90),\n" +
                    "(default, 'Other', 0.30);\n" +
                    "\n" +
                    "insert into grading_formats values\n" +
                    "(default, 'GPA'),\n" +
                    "(default, 'Letter Grade'),\n" +
                    "(default, 'Percentage'),\n" +
                    "(default, 'Pass/Fail'),\n" +
                    "(default, 'Presentation');\n" +
                    "\n" +
                    "insert into request_statuses values\n" +
                    "(default, 'Created'),\n" +
                    "(default, 'DS Approved'),\n" +
                    "(default, 'DH Approved'),\n" +
                    "(default, 'BC Approved'),\n" +
                    "(default, 'Reimbursed'),\n" +
                    "(default, 'Denied');\n" +
                    "\n" +
                    "insert into departments values\n" +
                    "(default, 'Operations'),\n" +
                    "(default, 'Human Resources'),\n" +
                    "(default, 'Sales'),\n" +
                    "(default, 'Benefits'),\n" +
                    "(default, 'Executives');");}
            query.executeUpdate();
            {sesh.save(dsOfDh);
            sesh.save(dh);
            sesh.save(ds);
            sesh.save(u);
            sesh.save(bcDh);
            sesh.save(bcDs);
            sesh.save(bc);

            sesh.save(dhOps);
            sesh.save(dhBenes);

            sesh.save(e1);
            sesh.save(e2);
            sesh.save(e3);

            sesh.save(r1);
            sesh.save(r2);
            sesh.save(r3);
            sesh.save(r4);}
            tx.commit();
        } catch (HibernateException e) {
            e.printStackTrace();
            if (tx != null) tx.rollback();
        } finally {
            sesh.close();
        }




    }

    @Test
    void addRequest() {

        Request expected = r1;

        Request actual = rd.addRequest(expected);

        expected.setRequestID(5);

        assertEquals(actual, expected);
    }

    @Test
    void getAllRequests() {

        List<Request> expectedList = new ArrayList<>();
        r1.setRequestID(1);
        expectedList.add(r1);
        r2.setRequestID(2);
        expectedList.add(r2);
        r3.setRequestID(3);
        expectedList.add(r3);
        r4.setRequestID(4);
        expectedList.add(r4);

        List<Request> actualList = rd.getAllRequests();

        assertEquals(actualList, expectedList);





    }

    @Test
    void getRequest() {

        Request expected = r1;
        r1.setRequestID(1);

        Request actual = rd.getRequest(1);

        assertEquals(actual, expected);



    }

    @Test
    void updateRequest() {

        r1.setRequestID(1);
        r1.setStatus(new RequestStatus(2, null));

        Request actual = rd.updateRequest(r1);

        r1.setStatus(new RequestStatus(2, "DS Approved"));

        Request expected = r1;

        assertEquals(actual, expected);

    }
}