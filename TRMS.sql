-----------------------------------------------
--DROPPERS--
		drop table if exists information_requests;
	drop table if exists requests;
drop table if exists request_statuses;

	drop table if exists events;
drop table if exists grading_formats;
drop table if exists event_types;

		drop table if exists department_heads;
	drop table if exists users;
drop table if exists departments;

-----------------------------------------------
--CREATORS--
create table if not exists departments (
	department_id SERIAL primary key,
	department_name VARCHAR (50)
);

create table if not exists users (
	user_id SERIAL primary key,
	username varchar (20),
	password varchar (20),
	first_name VARCHAR (50),
	last_name VARCHAR (50),
	department int references departments(department_id),
	supervisor int references users(user_id),
	start_date bigint
);

create table if not exists department_heads (
	department_id serial primary key references departments(department_id),
	department_head_id int references users(user_id)
);
	

create table if not exists grading_formats (
	format_id SERIAL primary key,
	format_type varchar (50) check (format_type in ('GPA', 'Letter Grade', 'Percentage', 'Pass/Fail', 'Presentation'))
);

create table if not exists event_types (
	event_type_id serial primary key,
	type_name varchar (50),
	reimbursement_coverage numeric (3,2)
);

create table if not exists events (
	event_id serial primary key,
	event_type int references event_types(event_type_id),
	date_time bigint,
	location varchar (50),
	cost numeric (7,2),
	description varchar (100),
	grading_format int references grading_formats(format_id),
	passing_grade varchar (50),
	missed_work varchar (500)
);

create table if not exists request_statuses (
	status_id serial primary key,
	status_name varchar (50)
);

create table if not exists requests (
	request_id serial primary key,
	creator int references users(user_id),
	event int references events(event_id),
	final_grade_or_presentation bytea,
	fgop_file_name varchar(100),
	assigned_bc int references users(user_id),
	approved_amount numeric (7,2),
	status int references request_statuses(status_id),
	
	date_created bigint,
	denial_reason varchar(500),
	
	first_name varchar(50),
	last_name varchar(50),
	department varchar(50),
	
	justification varchar (50),
	urgent boolean,
	amount_more_than_available boolean,
	tracking_number int
);

create table if not exists information_requests (
	information_request_id serial primary key,
	request_id int references requests(request_id),
	requesting_party int references users(user_id),
	responding_party int references users(user_id),
	request_body varchar (1000),
	response_body varchar (1000),
	request_date bigint
);

-----------------------------------------------
--POPULATORS--
	--REFERENCE TABLES--

insert into event_types values
(default, 'University Course', 0.80),
(default, 'Seminar', 0.60),
(default, 'Certification Preparation Class', 0.75),
(default, 'Certification', 1.00),
(default, 'Technical Training', 0.90),
(default, 'Other', 0.30);

insert into grading_formats values
(default, 'GPA'),
(default, 'Letter Grade'),
(default, 'Percentage'),
(default, 'Pass/Fail'),
(default, 'Presentation');

insert into request_statuses values
(default, 'Created'),
(default, 'DS Approved'),
(default, 'DH Approved'),
(default, 'BC Approved'),
(default, 'Reimbursed'),
(default, 'Denied');

insert into departments values
(default, 'Operations'),
(default, 'Human Resources'),
(default, 'Sales'),
(default, 'Benefits'),
(default, 'Executives');

	--MEAT TABLES--

insert into users values
--OPS
(default, 'sven55', 'password', 'Charles', 'Thompson', 1, null, 1634861835531),
(default, 'oldTown34', 'password', 'Alexis', 'Dewan', 1, null, 1634861835531), 
(default, 'punkRocker99', 'password', 'Ava', 'Asher', 1, null, 1600000000000), --DH --DS of 4
(default, 'masterz7', 'password', 'Michael', 'Zubek', 1, null, 1634861835531), --DS of 1, 2

--HR
(default, 'gnarly12', 'password', 'Steven', 'MacGuire', 2, null, 1634861835531),
(default, 'grunk77', 'password', 'Bernard', 'Jenkins', 2, null, 1600000000000), --DH --DS of 8
(default, 'liveFree4', 'password', 'Anita', 'Aguirre', 2, null, 1634861835531), 
(default, 'howdy_rowdy9', 'password', 'Michelle', 'Beckham', 2, null, 1634861835531), --DS of 5, 7

--SALES
(default, 'breathe5', 'password', 'Sam', 'Johnson', 3, null, 1634861835531),
(default, '88bedrock', 'password', 'John', 'Daniels', 3, null, 1634861835531),
(default, 'awfuleverything', 'password', 'Dale', 'Simpson', 3, null, 1600000000000), --DH --DS of 12
(default, 'thelastone', 'password', 'Delaney', 'Shields', 3, null, 1634861835531), --DS of 9, 10

--BENES
(default, 'almost10', 'password', 'Carla', 'Reyes', 4, null, 1634861835531),
(default, 'whysomany8', 'password', 'Megan', 'Glennen', 4, null, 1634861835531),
(default, 'doIt4', 'password', 'Madeline', 'Rogers', 4, null, 1600000000000), --DH --DS of 16
(default, 'justadude', 'password', 'Benjamin', 'Trumbo', 4, null, 1634861835531), --DS of 13, 14

--EXECS
(default, 'nobodyhome', 'password', 'David', 'Scott', 5, null, 1550000000000), --DH --DS of 17, 18
(default, 'direfire', 'password', 'Hannah', 'Wallace', 5, null, 1550000000000); --DS of 3, 6, 11, 15

--OPS
update users
set supervisor = 4
where user_id = 1 or user_id = 2;

update users
set supervisor = 3
where user_id = 4;

--HR
update users
set supervisor = 8
where user_id = 5 or user_id = 7;

update users
set supervisor = 6
where user_id = 8;

--SALES
update users
set supervisor = 12
where user_id = 9 or user_id = 10;

update users
set supervisor = 11
where user_id = 12;

--BENES
update users
set supervisor = 16
where user_id = 13 or user_id = 14;

update users
set supervisor = 15
where user_id = 16;

--EXECS
update users
set supervisor = 18
where user_id = 3 or user_id = 6 or user_id = 11 or user_id = 15;

update users
set supervisor = 17
where user_id = 17 or user_id = 18;

insert into department_heads values
(default, 3),
(default, 6),	
(default, 11),
(default, 15),
(default, 17);


--------------------------------
--PLAYGROUND--
--update requests 
--set fgop_file_name = null, final_grade_or_presentation = null
--where request_id = 1;








